<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_Block_Admin_Customer_Edit_Tab_Information extends Mage_Adminhtml_Block_Widget_Form {

	public function __construct() {
		parent::__construct();
	}

	public function initForm() {
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('_custom_options');
		$form->setFieldNameSuffix('custom_options');

        $customer = Mage::registry('current_customer');

     	$fieldset = $form->addFieldset('base_fieldset',
        	 array('legend'=>$this->__('Additional Registration Information'))
     	);

		$fieldset->getRenderer()->setTemplate('itoris/regfields/customer/fields.phtml');

     	$this->_setFieldset(array(), $fieldset);

		$settingsModel = Mage::getModel('itoris_regfields/settings');
		$store = $customer->getStore();
		$websiteId = $store->getWebsiteId();
		$storeId = $store->getId();
		$settingsModel->load($websiteId, $storeId);
		if ($settingsModel->getEnable() == Itoris_RegFields_Model_Settings::ENABLED) {
			/** @var $formModel Itoris_RegFields_Model_Form */
			$formModel = Mage::getModel('itoris_regfields/form');
			$sections = $formModel->getFormConfig($settingsModel->getActiveViewId($websiteId, $storeId));
			Mage::register('sections',$sections);
		}

     	$form->setValues($customer->getData());
     	$this->setForm($form);
     	return $this;
 	}
}
?>