<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_Block_Checkout_Form extends Mage_Checkout_Block_Onepage_Billing {

	protected $isItorisRegFormEnabledFlag = null;

	protected function isItorisRegFormEnabled() {
		if (is_null($this->isItorisRegFormEnabledFlag)) {

			if (!$this->isCustomerLoggedIn() && Mage::helper('itoris_regfields')->isRegisteredAutonomous(Mage::app()->getWebsite())) {
				$config = Mage::getConfig()->getModuleConfig('Itoris_QuickCheckout');
				if ($config->active == 'true') {
					$store = Mage::app()->getStore();
					$website = Mage::app()->getWebsite();
					$settings = Mage::getModel('itoris_quickcheckout/settings');
					$settings->load($website->getId(), $store->getId());
					if ($settings->getEnabled() && $this->getDataHelper()->isRegisteredAutonomous($website)) {
						$this->isItorisRegFormEnabledFlag = false;
						return false;
					}
				}
				/** @var $settingsModel Itoris_RegFields_Model_Settings */
				$settingsModel = $this->getDataHelper()->isEnabled();
				$this->isItorisRegFormEnabledFlag = $settingsModel ? true : false;
			}
		}

		return $this->isItorisRegFormEnabledFlag;
	}

	protected function _prepareLayout() {
		if ($this->isItorisRegFormEnabled()) {
			$this->getLayout()->getBlock('head')->addJs('itoris/regfields/js/main.js');
			$customerSession = Mage::getSingleton('customer/session');
			if ($customerSession->getAfterItorisRegForm()) {
				$customerSession->setAfterItorisRegForm(false);
				$this->getCheckout()->setStepData('billing', 'allow', true);$customerSession->setAfterItorisRegForm(false);
				$this->setOpenStep('billing');
			}
		}
		return parent::_prepareLayout();
	}

	protected function _toHtml() {
		$origHtml = parent::_toHtml();
		$formHtml = '';
		if ($this->isItorisRegFormEnabledFlag) {
			/** @var $formModel Itoris_RegFields_Model_Form */
			$formModel = Mage::getModel('itoris_regfields/form');
			$websiteId = Mage::app()->getWebsite()->getId();
			$storeId = Mage::app()->getStore()->getId();
			/** @var $settingsModel Itoris_RegFields_Model_Settings */
			$settingsModel = $this->getDataHelper()->isEnabled();
			$this->setFormConfig($formModel->getFormConfig($settingsModel->getActiveViewId($websiteId, $storeId)));
			$this->setTemplate('itoris/regfields/checkout/form.phtml');
			$formHtml = parent::_toHtml();
		}
		return $origHtml . $formHtml;
	}

	/**
	 * @return Itoris_RegFields_Helper_Data
	 */
	private function getDataHelper() {
		return Mage::helper('itoris_regfields');
	}
}
?>