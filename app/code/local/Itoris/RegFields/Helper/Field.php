<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_Helper_Field extends Mage_Core_Helper_Abstract {

	/** field types */
	const INPUT_BOX = 1;
	const PASSWORD_BOX = 2;
	const CHECKBOX = 3;
	const RADIO = 4;
	const SELECT_BOX = 5;
	const LIST_BOX = 6;
	const MULTISELECT_BOX = 7;
	const TEXTAREA = 8;
	const FILE = 9;
	const STATIC_TEXT = 10;
	const CAPTCHA = 11;
	const DATE = 12;

	/** validation types */
	const VALIDATION_EMAIL    = 'validate-email';
	const VALIDATION_NAME     = 'validate-name';
	const VALIDATION_NUMBER   = 'validate-digits';
	const VALIDATION_MONEY    = 'validate-money';
	const VALIDATION_PHONE    = 'validate-phone-number';
	const VALIDATION_DATE     = 'validate-date';
	const VALIDATION_ZIP      = 'validate-zip';

	/** captcha types */
	const ALIKON_MOD = 1;
	const CAPTCHA_FORM = 2;
	const SECUR_IMAGE = 3;

	private $addressCreatedFlag = false;
	private $addressShippingCreatedFlag = false;
	private $calendarLoadedFlag = false;
	private $regionCreatedFlag = false;
	private $countryCreatedFlag = false;
	private $regionShippingCreatedFlag = false;
	private $countryShippingCreatedFlag = false;

	private $arrayFieldValueMap = array();

	/**
	 * Prepare and return field html
	 *
	 * @param $config
	 * @param null $value
	 * @param null $sectionOrder
	 * @return string
	 */
	public function getFieldHtml($config, $value = null, $sectionOrder = null) {
		switch($config['type']) {
			case self::INPUT_BOX:
				return $this->getInputBoxHtml($config, $value);
			case self::PASSWORD_BOX:
				return $this->getPasswordBoxHtml($config, $value);
			case self::CHECKBOX:
				return $this->getCheckboxHtml($config, $value);
			case self::RADIO:
				return $this->getRadioBoxHtml($config, $value);
			case self::SELECT_BOX:
				return $this->getSelectBoxHtml($config, $value);
			case self::LIST_BOX:
				return $this->getListBoxHtml($config, $value);
			case self::MULTISELECT_BOX:
				return $this->getMultiSelectBoxHtml($config, $value);
			case self::TEXTAREA:
				return $this->getTextareaBoxHtml($config, $value);
			case self::STATIC_TEXT:
				return $this->getStaticTextBoxHtml($config);
			case self::FILE:
				return $this->getFileBoxHtml($config, $value);
			case self::CAPTCHA:
				return $this->getCaptchaBoxHtml($config, $sectionOrder);
			case self::DATE:
				return $this->getDateBoxHtml($config, $value);
		}
	}

	/**
	 * Prepare field html by field order for section.
	 * If section hasn't field with this order return empty field html.
	 *
	 * @param $section
	 * @param $fieldOrder
	 * @return string
	 */
	public function checkAndGetFieldHtml($section, $fieldOrder) {
		$fields = $section['fields'];
		$optionsValues = Mage::getSingleton('customer/session')->getCustomOptions();

		$arrayFieldValueMap = &$this->arrayFieldValueMap;

		for ($i = 0; $i < count($fields); $i++) {
			if ($fields[$i]['order'] == $fieldOrder) {
				if (isset($fields[$i]['name'])) {
					$fieldName = $fields[$i]['name'];
					if ($isArrayValue = strpos($fieldName, '[]')) {
						$fieldName = rtrim($fieldName, '[]');
					}
					if (is_array($optionsValues) && isset($optionsValues[$fieldName])) {
						$value = $optionsValues[$fieldName];
						if (is_array($value)) {
							if ($isArrayValue) {
								if (!isset($arrayFieldValueMap[$fieldName])) {
									$arrayFieldValueMap[$fieldName] = 0;
								} else {
									$arrayFieldValueMap[$fieldName] += 1;
								}

								$value = isset($value[$arrayFieldValueMap[$fieldName]]) ? $value[$arrayFieldValueMap[$fieldName]] : null;
							} else {
								$value = implode(',', $value);
							}
						}
					} else {
						$value = null;
					}
					return $this->getFieldHtml($fields[$i], $value, $section['order']);
				} else {
					return $this->getFieldHtml($fields[$i], null, $section['order']);
				}
			}
		}
		return $this->getEmptyFieldHtml();
	}

	private function getOptionValueByName($name) {
		$optionsValues = Mage::getSingleton('customer/session')->getCustomOptions();
		return isset($optionsValues[$name]) ? $optionsValues[$name] : null;
	}

	/**
	 * Get empty field html
	 *
	 * @return string
	 */
	private function getEmptyFieldHtml() {
		$html = '<div class="field empty-field"></div>';
		return $html;
	}

	private function prepareFieldName($name) {
		$preparedName = 'itoris[';
		if (strpos($name, '[]') !== false) {
			$preparedName .= substr($name, 0, strlen($name) - 2) . '][]';
		} else {
			$preparedName .= $name . ']';
		}

		return $preparedName;
	}

	/**
	 * Get field html for input box
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getInputBoxHtml($config, $value) {
		if ($config['name'] == 'prefix' || $config['name'] == 'suffix') {
			$prefixOptions = $config['name'] == 'prefix' ? $this->getPrefixOptions() : $this->getSuffixOptions();
			if (is_array($prefixOptions)) {
				$config['items'] = $this->convertToItemsOptions($prefixOptions);
				return $this->getSelectBoxHtml($config, $value);
			}
		}
		$this->addRegionUpdater($config);
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		if (isset($config['createAddress']) && $config['createAddress'] && !$this->addressCreatedFlag) {
			$html .= $this->addCreateAddressHtml();
		} elseif (isset($config['createAddressShipping']) && $config['createAddressShipping'] && !$this->addressShippingCreatedFlag) {
			$html .= $this->addCreateAddressHtml(true);
		}
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		if (!isset($config['removable']) || !$config['removable']) {
			$name = $config['name'];
		} else {
			$name = $this->prepareFieldName($config['name']);
		}
		$html .= '<input name="' . $name . '" type="text" id="' . $config['name'] . '" class="input-text ';
		if (isset($config['validation'])) {
			$html .= $config['validation'];
		}
		if (isset($config['css_class'])) {
			$html .= ' ' . $config['css_class'] . ' ';
		}
		if (isset($config['required']) && $config['required']) {
			$html .= ' required-entry ';
		}
		$html .= '"';
		if ($value) {
			$html .= ' value="' . $value . '"';
		} elseif (isset($config['default_value'])) {
			$html .= ' value="' . $config['default_value'] . '"';
		}
		if (isset($config['html_arg'])) {
			$html .= ' ' . $config['html_arg'];
		}
		$html .= '/>';
		$html .= '</div></div>';
		return $html;
	}

	protected function convertToItemsOptions($options) {
		$items = array();
		$customerHelper = $this->getCustomerHelper();
		foreach ($options as $option) {
			$items[] = array(
				'value' => $option,
				'label' => $customerHelper->__($option),
			);
		}

		return $items;
	}

	private function addCreateAddressHtml($isShipping = false) {
		if ($isShipping) {
			$this->addressShippingCreatedFlag = true;
			$html = '<input type="hidden" name="default_shipping" value="1"/>';
		} else {
			$this->addressCreatedFlag = true;
			$html = '<input type="hidden" name="default_billing" value="1"/>';
		}
		return '<input type="hidden" name="create_address" value="1"/>' . $html;
	}

	/**
	 * Get field html for password box
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getPasswordBoxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		if (!isset($config['removable']) || !$config['removable']) {
			$name = $config['name'];
		} else {
			$name = 'itoris[' . $config['name'] . ']';
		}
		$html .= '<input name="' . $name . '" type="password" id="' . $config['name'] . '" class="input-text validate-password';
		if (isset($config['css_class'])) {
			$html .= ' ' . $config['css_class'] . ' ';
		}
		if (isset($config['required']) && $config['required']) {
			$html .= ' required-entry ';
		}
		$html .= '"';
		if (isset($config['html_arg'])) {
			$html .= ' ' . $config['html_arg'];
		}
		$html .= '/>';
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for checkboxes
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getCheckboxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		$html .= '<label';
		if ((isset($config['required']) && $config['required'])
			|| (isset($config['min_required']) && $config['min_required'])
		) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$label = isset($config['label']) ? $config['label'] : '';
		$html .= $label . '</label><br/>';
		$html .= '<div class="input-box">';
		if ($value) {
			$value = explode(',', $value);
		}
		foreach ($config['items'] as $item) {
			if (!isset($config['removable']) || !$config['removable']) {
				$name = $config['name'];
			} else {
				$name = 'itoris[' . $config['name'] . '][]';
			}
			$html .= '<input name="' . $name . '" type="checkbox" class="';
			if (isset($config['css_class'])) {
				$html .= ' ' . $config['css_class'] . ' ';
			}
			if (isset($config['min_required']) && $config['min_required']) {
				$html .= ' validate-one-required-by-name';
			}
			$html .= '"';
			if (isset($config['html_arg'])) {
				$html .= ' ' . $config['html_arg'];
			}
			if ($value) {
				if (in_array($item['value'], $value)) {
					$html .= ' checked="checked"';
				}
			} elseif (isset($item['selected']) && $item['selected']) {
				$html .= ' checked="checked"';
			}
			$html .= ' value="'. $item['value'] .'"/><label class="float-none">' . $item['label'] . '</label><br/>';
		}
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for radios
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getRadioBoxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label><br/>';
		$html .= '<div class="input-box">';
		foreach ($config['items'] as $item) {
			$html .= '<input name="itoris[' . $config['name'] . ']" type="radio" class="';
			if (isset($config['css_class'])) {
				$html .= ' ' . $config['css_class'] . ' ';
			}
			if (isset($config['required']) && $config['required']) {
				$html .= ' validate-one-required-by-name';
			}
			$html .= '"';
			if (isset($config['html_arg'])) {
				$html .= ' ' . $config['html_arg'];
			}
			if ($value) {
				if ($value == $item['value']) {
					$html .= ' checked="checked"';
				}
			} elseif (isset($item['selected']) && $item['selected']) {
				$html .= ' checked="checked"';
			}
			$html .= ' value="'. $item['value'] .'"/><label class="float-none">' . $item['label'] . '</label><br/>';
		}
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for select box
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getSelectBoxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		if (isset($config['createAddress']) && $config['createAddress'] && !$this->addressCreatedFlag) {
			$html .= $this->addCreateAddressHtml();
		} elseif (isset($config['createAddressShipping']) && $config['createAddressShipping'] && !$this->addressShippingCreatedFlag) {
			$html .= $this->addCreateAddressHtml(true);
		}
		$this->addRegionUpdater($config);
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		if ($config['name'] == 'region_id' || $config['name'] == 's_region_id') {
			$regionInputId =  $config['name'] == 's_region_id' ? 's_region' : 'region';
			$html .= '<input type="text" id="'.$regionInputId.'" value="'. $this->getOptionValueByName($regionInputId) .'" name="itoris['.$regionInputId.']" title="' . $this->__('State/Province') . '" class="input-text" style="display:none;" />';
		}
		$html .= '<select name="itoris['.$config['name'].']" id="' . $config['name'] . '" class="input-text ';
		if (isset($config['css_class'])) {
			$html .= ' ' . $config['css_class'] . ' ';
		}
		if (isset($config['required']) && $config['required']) {
			$html .= ' validate-select';
		}
		$html .= '"';
		if (isset($config['html_arg'])) {
			$html .= ' ' . $config['html_arg'];
		}
		$html .= '>';
		$html .= '<option value="none">' . $this->__('--Please select--') . '</option>';
		$items = $config['items'];
		if ($config['name'] == 'region_id') {
			$regions = $this->getCountryRegions();
			if (!empty($regions)) {
				$items = $regions;
			}
		}
		if ($config['name'] == 'country_id' || $config['name'] == 's_country_id') {
			$items = $this->getCountryOptions(true);
			if (is_null($value)) {
				$defaultCountryCode = Mage::getStoreConfig('general/country/default');
				if ($defaultCountryCode) {
					foreach ($items as &$_item) {
						if ($_item['value'] == $defaultCountryCode) {
							$_item['selected'] = true;
						}
					}					
				}
			}
		}
		foreach ($items as $item) {
			if (isset($item['value']) && $item['value']) {
				$html .= '<option ';
				if ($value) {
					if ($value == $item['value']) {
						$html .= ' selected="selected"';
					}
				} elseif (isset($item['selected']) && $item['selected']) {
					$html .= ' selected="selected"';
				}
				$html .= ' value="'. $item['value'] .'">' . $item['label'] . '</option>';
			}
		}
		$html .= '</select>';
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for list box
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getListBoxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		$html .= '<select name="itoris['.$config['name'].']" id="' . $config['name'] . '" size="' . $config['size'] . '" class="input-text ';
		if (isset($config['css_class'])) {
			$html .= ' ' . $config['css_class'] . ' ';
		}
		if (isset($config['required']) && $config['required']) {
			$html .= ' validate-select';
		}
		$html .= '"';
		if (isset($config['html_arg'])) {
			$html .= ' ' . $config['html_arg'];
		}
		$html .= '>';
		$html .= '<option value="none">' . $this->__('--Please select--') . '</option>';
		foreach ($config['items'] as $item) {
			$html .= '<option ';
			if ($value) {
				if ($value == $item['value']) {
					$html .= ' selected="selected"';
				}
			} elseif (isset($item['selected']) && $item['selected']) {
				$html .= ' selected="selected"';
			}
			$html .= ' value="'. $item['value'] .'">' . $item['label'] . '</option>';
		}
		$html .= '</select>';
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for multiselect box
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getMultiSelectBoxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		$html .= '<select name="itoris['.$config['name'].'][]" id="' . $config['name'] . '" size="' . $config['size'] . '" multiple="multiple" class="input-text ';
		if (isset($config['css_class'])) {
			$html .= ' ' . $config['css_class'] . ' ';
		}
		if (isset($config['required']) && $config['required']) {
			$html .= ' validate-select';
		}
		$html .= '"';
		if (isset($config['html_arg'])) {
			$html .= ' ' . $config['html_arg'];
		}
		$html .= '>';
		if ($value) {
			$value = explode(',', $value);
		}
		$html .= '<option value="none">' . $this->__('--Please select--') . '</option>';
		foreach ($config['items'] as $item) {
			$html .= '<option ';
			if ($value) {
				if (in_array($item['value'], $value)) {
					$html .= ' selected="selected"';
				}
			} elseif (isset($item['selected']) && $item['selected']) {
				$html .= ' selected="selected"';
			}
			$html .= ' value="'. $item['value'] .'">' . $item['label'] . '</option>';
		}
		$html .= '</select>';
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for textarea
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getTextareaBoxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		$html .= '<textarea name="itoris[' . $config['name'] . ']" id="' . $config['name'] . '" rows="' . $config['rows'] . '" id="' . $config['name'] . '" class="input-text ';
		if (isset($config['validation'])) {
			$html .= $config['validation'];
		}
		if (isset($config['css_class'])) {
			$html .= ' ' . $config['css_class'] . ' ';
		}
		if (isset($config['required']) && $config['required']) {
			$html .= ' required-entry ';
		}
		$html .= '" ';
		if (isset($config['html_arg'])) {
			$html .= ' ' . $config['html_arg'];
		}
		$html .= '>';
		if ($value) {
			$html .= $value;
		} elseif (isset($config['default_value']) && !$value) {
			$html .= $config['default_value'];
		}
		$html .= '</textarea>';
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for static text
	 *
	 * @param $config
	 * @return string
	 */
	private function getStaticTextBoxHtml($config) {
		$html = '<div class="field">';
		$html .= '<div class="input-box ';
		if (isset($config['css_class'])) {
			$html .= $config['css_class'];
		}
		$html .= '" ';
		if (isset($config['html_arg'])) {
			$html .= $config['html_arg'];
		}

		$html .= ' >';
		$html .= $config['static_text'];
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for file box
	 *
	 * @param $config
	 * @param $value
	 * @return string
	 */
	private function getFileBoxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		if ($value && $value != 'null') {
			$value = unserialize($value);
			$fileName = $value['file'];
			//var_dump($fileName);die;
			$html .= '<a class="link-file" href="'. Mage::getUrl('itoris_regfields/uploader/uploadFile', array('_nosid' => true)) . '?file='.$fileName.'">' . $value['name'] . '</a>';
			$html .= '<span class="link-wishlist" onclick="ItorisHelper.showFileInput(\'' . $config['name'] . '\',\''. $this->__('Do you really want to remove this file?') .'\', '. ((isset($config['required']) && $config['required']) ? 'true' : 'false') .')">('. $this->__('remove') .')</span>';
		}
		$html .= '<input name="itoris[' . $config['name'] . ']" type="hidden" id="itoris_file_value_' . $config['name'] . '" value="'. (($value && $value != 'null') ? 'itoris_field_has_file' : 'null') .'" />';
		$html .= '<input name="itoris[' . $config['name'] . ']" type="file" id="itoris_file_' . $config['name'] . '" class="input-text ';
		if (isset($config['css_class'])) {
			$html .= ' ' . $config['css_class'] . ' ';
		}
		if (isset($config['required']) && $config['required'] && !($value && $value != 'null')) {
			$html .= 'required-file';
		}
		$html .= '"';
		if (isset($config['html_arg'])) {
			$html .= ' ' . $config['html_arg'];
		}
		if ($value && $value != 'null') {
			$html .= 'style="display:none;"';
		}
		if (isset($config['file_extensions'])) {
			/** @var $mimeHelper Itoris_RegFields_Helper_Mime */
			$mimeHelper = Mage::helper('itoris_regfields/mime');
			$mimeTypes = array();
			foreach (explode(',', $config['file_extensions']) as $mimeType) {
				$newMimeType = $mimeHelper->getMimeType(trim($mimeType));
				if (!in_array($newMimeType, $mimeTypes)) {
					$mimeTypes[] = $newMimeType;
				}
			}
			$mimeTypes = implode(', ', $mimeTypes);
			$html .= ' accept="'. $mimeTypes .'"';
		}
		$html .= '/>';
		if (isset($config['file_extensions'])) {
			$html .= '<br/><span class="note">'. $this->__('File Extensions Allowed') .': ' . $config['file_extensions'] . '</span>';
		}
		if (isset($config['max_file_size'])) {
			$html .= '<br/><span class="note">'. $this->__('Max file size in bytes') .': ' . $config['max_file_size'] . '</span><br/>';
		}
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get field html for captcha box
	 *
	 * @param $config
	 * @param $sectionOrder
	 * @return string
	 */
	private function getCaptchaBoxHtml($config, $sectionOrder) {
		$html = '<div class="field">';
		$html .= '<label class="required">';
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		switch($config['captcha']){
			case self::ALIKON_MOD:
						$url = 'alikon';
						break;
			case self::SECUR_IMAGE:
						$url = 'securimage';
						break;
			case self::CAPTCHA_FORM:
						$url = 'captchaform';
						break;
		}
		$baseUrl = Mage::getUrl('itoris_regfields', array('_nosid' => true));
		$imgId = $config['order'];
		$html .= '<img id="captcha_' . $imgId . '" src="'. $baseUrl .'captcha/' . $url . '?' . $imgId . '" alt="CAPTCHA Image" />
				<div onclick="ItorisHelper.reloadCaptcha(\'captcha_' . $imgId . '\', \''.$url.'\');return false;" class="reload-captcha" title="'. $this->__('Reload the Image') .'"></div>
				<br/><span class="note">'. $this->__('Please, enter the text shown in the image into the field below') .'</span><br/>';
		$html .= '<input type="text" name="captcha['.$url.'_' . $sectionOrder . '_' . $imgId . ']" class="required-entry" size="10" maxlength="10" />';
		$html .= '<script type="text/javascript">ItorisHelper.baseUrl = \'' . $baseUrl . '\';</script>';
		$html .= '</div></div>';
		return $html;
	}

	public function getDateBoxHtml($config, $value) {
		$html = '<div class="field" id="' . $config['name'] . '_box">';
		if (!$this->calendarLoadedFlag) {
			$this->calendarLoadedFlag = true;
		}
		$html .= '<label for="' . $config['name'] . '"';
		if (isset($config['required']) && $config['required']) {
			$html .= ' class="required"><em>*</em>';
		} else {
            $html .= '>';
        }
		$html .= (isset($config['label']) ? $config['label'] : '') . '</label>';
		$html .= '<div class="input-box">';
		$name = $this->prepareFieldName($config['name']);
		$html .= '<input name="' . $name . '" type="text" id="' . $config['name'] . '" class="input-text date ';
		if (isset($config['validation'])) {
			$html .= $config['validation'];
		}
		if (isset($config['css_class'])) {
			$html .= ' ' . $config['css_class'] . ' ';
		}
		if (isset($config['required']) && $config['required']) {
			$html .= ' required-entry ';
		}
		$html .= '"';
		if ($value) {
			$html .= ' value="' . $value . '"';
		} elseif (isset($config['default_value'])) {
			$html .= ' value="' . $config['default_value'] . '"';
		}
		if (isset($config['html_arg'])) {
			$html .= ' ' . $config['html_arg'];
		}
		$html .= '/>';
		$html .= '<img id="'. $config['name'] .'_trig" class="date-trig" title="' . $this->__('Select Date') . '" alt="' . $this->__('Select Date') . '" src="' . Mage::getBaseUrl('skin') . 'adminhtml/default/default/images/grid-cal.gif' . '"/>';
		$dateFormat = Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
		$html .= "<script type=\"text/javascript\">Calendar.setup({
								inputField: '{$config['name']}',
								ifFormat: '{$dateFormat}',
								showsTime: false,
								button: '{$config['name']}_trig',
								singleClick: true
							});
		</script>";
		$html .= '</div></div>';
		return $html;
	}

	/**
	 * Get custom fields html for the customer for frontend or backend
	 *
	 * @param $config
	 * @param null $customerId
	 * @param bool $backend
	 * @return string
	 */
	public function getCustomFieldsHtml($config, $customerId = null, $backend = false, $withoutDefault = false) {
		$html = '';
		/** @var $customerOptionModel Itoris_RegFields_Model_Customer */
		$customerOptionModel = Mage::getModel('itoris_regfields/customer');
		if (!$customerId && !$backend) {
			$customerId = Mage::getSingleton('customer/session')->getCustomerId();
		}
		foreach ($config as $section) {
			if (isset($section['fields'])) {
				foreach($section['fields'] as $field) {
					if ($withoutDefault && isset($field['isDefault']) && $field['isDefault']) {
						continue;
					}
					if ($field['type'] != self::CAPTCHA && $field['type'] != self::STATIC_TEXT) {
						if (isset($field['removable']) && $field['removable']) {
							$html .= $backend ? '' : '<li>';
							$value = is_null($customerId) ? null : $customerOptionModel->loadOption($field['name'], $customerId);
							$html .= $this->getFieldHtml($field, $value);
							$html .= $backend ? '' : '</li>';
							$customerOptionModel->unsetData();
						}
					}
				}
			}
		}
		return $html;
	}

	public function getFieldTypesJson() {
		$fieldTypes = array(
			'input_box'        => self::INPUT_BOX,
			'password_box'     => self::PASSWORD_BOX,
			'checkbox'         => self::CHECKBOX,
			'radio'            => self::RADIO,
			'select_box'       => self::SELECT_BOX,
			'list_box'         => self::LIST_BOX,
			'multiselect_box'  => self::MULTISELECT_BOX,
			'textarea'         => self::TEXTAREA,
			'file'             => self::FILE,
			'static_text'      => self::STATIC_TEXT,
			'captcha'          => self::CAPTCHA,
			'date'             => self::DATE,
		);
		return Zend_Json::encode($fieldTypes);
	}

	public function getValidationTypesJson() {
		$validationTypes = array(
			'please_select' => 0,
			'email'    => self::VALIDATION_EMAIL,
			'name'     => self::VALIDATION_NAME,
			'number'   => self::VALIDATION_NUMBER,
			'money'    => self::VALIDATION_MONEY,
			'phone'    => self::VALIDATION_PHONE,
			'date'     => self::VALIDATION_DATE,
			'zip'      => self::VALIDATION_ZIP,
		);
		return Zend_Json::encode($validationTypes);
	}

	public function getCaptchaTypesJson() {
		$captchaTypes = array(
			'alikon_mod'   => self::ALIKON_MOD,
			'captcha_form' => self::CAPTCHA_FORM,
			'secur_image'  => self::SECUR_IMAGE,
		);
		return Zend_Json::encode($captchaTypes);
	}

	/*
	 * Return configuration of the fields of the standard Magento Login Form
	 *
	 * @return array
	 */
	public function getDefaultSections() {
		$sections = array(
			array(
				'label'     => $this->__('Personal Information'),
				'cols'      => 2,
				'rows'      => 3,
				'order'     => 1,
				'removable' => false,
				'fields'    => array(
					array(
						'type'      => self::INPUT_BOX,
						'label'     => $this->__('First Name'),
						'name'      => 'firstname',
						'required'  => true,
						'removable' => false,
						'order'     => 1,
					),
					array(
						'type'      => self::INPUT_BOX,
						'label'     => $this->__('Last Name'),
						'name'      => 'lastname',
						'required'  => true,
						'removable' => false,
						'order'     => 2,
					),
					array(
						'type'       => self::INPUT_BOX,
						'label'      => $this->__('Email Address'),
						'name'       => 'email',
						'required'   => true,
						'validation' => self::VALIDATION_EMAIL,
						'removable'  => false,
						'order'      => 3,
					),
					array(
						'type'       => self::CHECKBOX,
						'required'   => false,
						'quantity'   => 1,
						'name'  => 'is_subscribed',
						'removable'  => false,
						'items'      => array(
							array(
								'label' => $this->__('Sing Up for Newsletter'),
								'order' => 1,
								'value' => 1,
							),
						),
						'order'      => 5,
					),
				),
			),
			array(
				'label'     => $this->__('Login Information'),
				'cols'      => 2,
				'rows'      => 1,
				'order'     => 2,
				'removable' => false,
				'fields'    => array(
					array(
						'type'      => self::PASSWORD_BOX,
						'label'     => $this->__('Password'),
						'name'      => 'password',
						'required'  => true,
						'removable' => false,
						'order'     => 1,
					),
					array(
						'type'      => self::PASSWORD_BOX,
						'label'     => $this->__('Confirm Password'),
						'name'      => 'confirmation',
						'required'  => true,
						'removable' => false,
						'order'     => 2,
					),
				),
			),
		);
		return $sections;
	}

	/**
	 * Validate custom fields values
	 *
	 * @param $values
	 * @param $config
	 * @return array
	 */
	public function validate($values, $config, $excludeDefaultFields = false) {
		$errors = array();
		foreach ($config as $section) {
			if ($section['fields']) {
				foreach ($section['fields'] as $field) {
					if ($excludeDefaultFields && isset($field['isDefault']) && $field['isDefault']) {
						continue;
					}
					if ( (isset($field['required']) && $field['required'])
						|| (isset($field['min_required']) && $field['min_required'])
						|| (isset($field['name']) && isset($values[$field['name']]) && !empty($values[$field['name']]))
					) {
						$error = $this->validateField($values, $field);
						if ($error) {
							$errors[] = $error;
						}
					}
				}
			}
		}

		return $errors;
	}

	/**
	 * Validate field value
	 *
	 * @param $values
	 * @param $fieldConfig
	 * @return bool|null|string
	 */
	public function validateField($values, $fieldConfig) {
		if (!isset($fieldConfig['removable']) || (isset($fieldConfig['removable']) && !$fieldConfig['removable'])) {
			return false;
		}
		if (isset($fieldConfig['required']) && $fieldConfig['required'] && !isset($values[$fieldConfig['name']])) {
			return (isset($fieldConfig['label']) ? $fieldConfig['label'] : $fieldConfig['name']) . $this->__(' is required');
		}
		if (isset($fieldConfig['min_required']) && (int)$fieldConfig['min_required'] && $fieldConfig['type'] == self::CHECKBOX) {
			if (!isset($values[$fieldConfig['name']]) || count($values[$fieldConfig['name']]) < (int)$fieldConfig['min_required']) {
				return (isset($fieldConfig['label']) ? $fieldConfig['label'] : $fieldConfig['name']) . $this->__(' is required') . ' (' . $this->__('minimum') . ' '. $fieldConfig['min_required'] . ' ' . $this->__('selections') . ')';
			}
		}
		if (isset($fieldConfig['validation']) && isset($values[$fieldConfig['name']])) {
			$error = null;
			switch ($fieldConfig['validation']) {
				case self::VALIDATION_EMAIL:
						$error = $this->validateEmail($values[$fieldConfig['name']]);
						break;
				case self::VALIDATION_NUMBER:
						$error = $this->validateNumber($values[$fieldConfig['name']]);
						break;
				case self::VALIDATION_PHONE:
						$error = $this->validatePhone($values[$fieldConfig['name']]);
						break;
				case self::VALIDATION_MONEY:
						$error = $this->validateMoney($values[$fieldConfig['name']]);
						break;
				case self::VALIDATION_NAME:
						$error = $this->validateName($values[$fieldConfig['name']]);
						break;
			}
			if ($error) {
				return $error;
			}
		}
		switch ($fieldConfig['type']) {
			case self::FILE:
				return $this->validateFile($values[$fieldConfig['name']], $fieldConfig);

		}
		return false;
	}

	private function validateName($name) {
		if (preg_match("/^[a-zA-Z-\s']+$/", $name)) {
			return false;
		} else {
			return $name . ' ' . $this->__('is not a valid name');
		}
	}

	private function validateMoney($value) {
		if (preg_match('/^([0-9]*|([0-9]{0,3},[0-9]{3})*)(\.[0-9]{2})?$/', $value)) {
			return false;
		} else {
			return $value . ' ' . $this->__('is not a valid money format');
		}
	}

	private function validatePhone($phone) {
		if (preg_match('/^[0-9-\s\.\(\)\+]+$/', $phone)) {
			return false;
		} else {
			return $phone . ' ' . $this->__('is not a valid phone');
		}
	}

	private function validateNumber($num) {
		$validator = new Zend_Validate_Int();
		if ($validator->isValid($num)) {
			return false;
		} else {
			return $num . ' ' . $this->__('is not a number');
		}
	}

	private function validateEmail($email) {
		$validator = new Zend_Validate_EmailAddress();
		if ($validator->isValid($email)) {
			return false;
		} else {
			return $email . ' ' . $this->__('is not a valid email address');
		}
	}

	public function validateFile($value, $fieldConfig) {
		if ($value == 'null') {
			if (isset($fieldConfig['required']) && $fieldConfig['required']) {
				return $this->__('Some files have not been uploaded');
			} else {
				return false;
			}
		}
		if ($value == 'itoris_field_has_file') {
			return false;
		}
		$value = unserialize($value);
		if (isset($fieldConfig['max_file_size']) && ($fieldConfig['max_file_size'] < $value['size'])) {
			return $this->__('The file size must be less than ' . $fieldConfig['max_file_size'] . ' bytes.');
		}
		if (isset($fieldConfig['file_extensions'])) {
			$allowedMimeTypes = array();
			foreach (explode(',', $fieldConfig['file_extensions']) as $type) {
				$mimeType = $this->getMimeHelper()->getMimeType(trim($type));
				if ($mimeType) {
					$allowedMimeTypes[] = $mimeType;
				}
			}
			$checkMime = $value['mime'];
			if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
				if ($value['mime'] == 'image/pjpeg') {
					$checkMime = 'image/jpeg';
				} elseif ($value['mime'] == 'image/x-png') {
					$checkMime = 'image/png';
				}
			}
			if (!in_array($checkMime, $allowedMimeTypes)) {
				return $this->__('Incorrect file format. Allowed formats are:' . ' ' . strtoupper($fieldConfig['file_extensions']));
			}
		}
		return false;
	}

	public function getAdditionalDefaultFields() {
		$genderOptions = Mage::getResourceSingleton('customer/customer')->getAttribute('gender')->getSource()->getAllOptions();
		$genderOptions = $this->deleteEmptyOptionsAndAddOrder($genderOptions);
		//$countryOptions = $this->getCountryOptions();
		//$countryOptions = $this->deleteEmptyOptionsAndAddOrder($countryOptions, 'US');
		//$regionOptions = $this->getRegionOptions();
		//$regionOptions = $this->deleteEmptyOptionsAndAddOrder($regionOptions);
		$groupOptions = $this->getGroupOptions();
		$groupOptions = $this->deleteEmptyOptionsAndAddOrder($groupOptions);

		$emptyOptions = array(array('value' => '1', 'label' => Mage::helper('adminhtml')->__('-- Please Select --')));
		$fields = array(
			'prefix' => array(
				'label'     => $this->__('Prefix'),
				'name'      => 'prefix',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
			),
			'middlename' => array(
				'label'     => $this->__('Middle Name/Initial'),
				'name'      => 'middlename',
				'required'  => false,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
			),
			'suffix' => array(
				'label'     => $this->__('Suffix'),
				'name'      => 'suffix',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
			),
			'dob' => array(
				'label'      => $this->__('Date of Birth'),
				'name'       => 'dob',
				'required'   => true,
				'type'       => self::DATE,
				'removable'  => true,
				'isDefault'  => true,
				'validation' => self::DATE,
			),
			'taxvat' => array(
				'label'     => $this->__('Tax/VAT number'),
				'name'      => 'taxvat',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
			),
			'gender' => array(
				'label'     => $this->__('Gender'),
				'name'      => 'gender',
				'required'  => true,
				'type'      => self::SELECT_BOX,
				'removable' => true,
				'isDefault' => true,
				'items'     => $genderOptions,
			),
			'group' => array(
				'label'     => $this->__('Group'),
				'name'      => 'group_id',
				'required'  => true,
				'type'      => self::SELECT_BOX,
				'removable' => true,
				'isDefault' => true,
				'items'     => $groupOptions,
			),
			'company' => array(
				'label'     => $this->__('Billing Company'),
				'name'      => 'company',
				'required'  => false,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
				'createAddress' => true,
			),
			'telephone' => array(
				'label'     => $this->__('Billing Telephone'),
				'name'      => 'telephone',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
				'createAddress' => true,
				'validation'    => self::VALIDATION_PHONE,
			),
			/*'fax' => array(
				'label'     => $this->__('Fax'),
				'name'      => 'fax',
				'required'  => false,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
			),*/
			'street' => array(
				'label'     => $this->__('Billing Street Address'),
				'name'      => 'street[]',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
				'createAddress' => true,
			),
			'city' => array(
				'label'     => $this->__('Billing City'),
				'name'      => 'city',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
				'createAddress' => true,
			),
			'country' => array(
				'label'     => $this->__('Billing Country'),
				'name'      => 'country_id',
				'required'  => true,
				'type'      => self::SELECT_BOX,
				'removable' => true,
				'isDefault' => true,
				'items'     => $emptyOptions,//$countryOptions,
				'createAddress' => true,
			),
			'region' => array(
				'label'     => $this->__('Billing State/Province'),
				'name'      => 'region_id',
				'required'  => true,
				'type'      => self::SELECT_BOX,
				'removable' => true,
				'isDefault' => true,
				'items'     => $emptyOptions,//$regionOptions,
				'createAddress' => true,
			),
			'postcode' => array(
				'label'      => $this->__('Billing Zip/Postal Code'),
				'name'       => 'postcode',
				'required'   => true,
				'requiredFixed' => true,
				'type'       => self::INPUT_BOX,
				'removable'  => true,
				'isDefault'  => true,
				'validation' => self::VALIDATION_ZIP,
				'createAddress' => true,
			),
			'fax' => array(
				'label'      => $this->__('Billing Fax'),
				'name'       => 'fax',
				'required'   => false,
				'type'       => self::INPUT_BOX,
				'removable'  => true,
				'isDefault'  => true,
				'createAddress' => true,
			),
			's_company' => array(
				'label'     => $this->__('Shipping Company'),
				'name'      => 's_company',
				'required'  => false,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
				'createAddressShipping' => true,
			),
			's_telephone' => array(
				'label'     => $this->__('Shipping Telephone'),
				'name'      => 's_telephone',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
				'createAddressShipping' => true,
				'validation'    => self::VALIDATION_PHONE,
			),
			's_street' => array(
				'label'     => $this->__('Shipping Street Address'),
				'name'      => 's_street[]',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
				'createAddressShipping' => true,
			),
			's_city' => array(
				'label'     => $this->__('Shipping City'),
				'name'      => 's_city',
				'required'  => true,
				'type'      => self::INPUT_BOX,
				'removable' => true,
				'isDefault' => true,
				'createAddressShipping' => true,
			),
			's_country' => array(
				'label'     => $this->__('Shipping Country'),
				'name'      => 's_country_id',
				'required'  => true,
				'type'      => self::SELECT_BOX,
				'removable' => true,
				'isDefault' => true,
				'items'     => $emptyOptions,//$countryOptions,
				'createAddressShipping' => true,
			),

			's_region' => array(
				'label'     => $this->__('Shipping State/Province'),
				'name'      => 's_region_id',
				'required'  => true,
				'type'      => self::SELECT_BOX,
				'removable' => true,
				'isDefault' => true,
				'items'     => $emptyOptions,//$regionOptions,
				'createAddressShipping' => true,
			),

			's_postcode' => array(
				'label'      => $this->__('Shipping Zip/Postal Code'),
				'name'       => 's_postcode',
				'required'   => true,
				'requiredFixed' => true,
				'type'       => self::INPUT_BOX,
				'removable'  => true,
				'isDefault'  => true,
				'validation' => self::VALIDATION_ZIP,
				'createAddressShipping' => true,
			),
			's_fax' => array(
				'label'      => $this->__('Shipping Fax'),
				'name'       => 's_fax',
				'required'   => false,
				'type'       => self::INPUT_BOX,
				'removable'  => true,
				'isDefault'  => true,
				'createAddressShipping' => true,
			),
		);

		return $fields + $this->getAdditionalCustomerAttributes();
	}

	protected function getAdditionalCustomerAttributes() {
		$additionalAttributes = array();
		/** @var $customer Mage_Customer_Model_Customer */
		$customer = Mage::getModel('customer/customer');
		/** @var $attr Mage_Eav_Model_Entity_Attribute */
		foreach ($customer->getAttributes() as $attr) {
			if (!is_null($attr->getIsSystem()) && !$attr->getIsSystem() && $attr->getAttributeCode() != 'created_at') {
				try {
					$tempData = array(
						'label'     => $attr->getFrontendLabel(),
						'name'      => $attr->getAttributeCode(),
						'required'  => $attr->getIsRequired(),
						'removable' => true,
						'isDefault' => true,
						'type'      => $this->getTypeId($attr->getFrontendInput()),
					);
					if ($tempData['type'] == self::SELECT_BOX) {
						$tempData['items'] = $this->deleteEmptyOptionsAndAddOrder($attr->getSource()->getAllOptions());
					}
					$additionalAttributes[$attr->getAttributeCode()] = $tempData;
				} catch (Exception $e) {
					Mage::logException($e);
				}
			}
		}

		return $additionalAttributes;
	}

	public function getTypeId($type) {
		switch ($type) {
			case 'password':
				return self::PASSWORD_BOX;
			case 'textarea':
				return self::TEXTAREA;
			case 'date':
				return self::DATE;
			case 'select':
				return self::SELECT_BOX;
			case 'text':
			default:
				return self::INPUT_BOX;
		}
	}

	public function getCountryOptions($byStore = false) {
		$collection = Mage::getModel('directory/country')->getResourceCollection();
		if ($byStore) {
			$collection->loadByStore();
		}

		return $collection->toOptionArray();
	}

	private function getRegionOptions() {
		$collection = Mage::getModel('directory/region')->getResourceCollection()
			->addCountryFilter('US')
			->load();

		return $collection->toOptionArray();
	}

	private function getGroupOptions() {
		/** @var $sourceModel Mage_Adminhtml_Model_System_Config_Source_Customer_Group */
		$sourceModel = Mage::getModel('adminhtml/system_config_source_customer_group');

		return $sourceModel->toOptionArray();
	}

	private function deleteEmptyOptionsAndAddOrder($options, $defaultValue = null) {
		foreach ($options as $key => $value) {
			if ((bool)$value['value']) {
				$options[$key]['order'] = $key;
				if ($defaultValue && $options[$key]['value'] == $defaultValue) {
					$options[$key]['selected'] = 1;
				}
			} else {
				$options[$key] = array();
			}
		}

		return array_values($options);
	}

	public function isAddressCreated($isShipping = false) {
		return $isShipping ? $this->addressShippingCreatedFlag : $this->addressCreatedFlag;
	}

	public function canUseRegionUpdater($isShipping = false) {
		if ($isShipping) {
			return ($this->regionShippingCreatedFlag && $this->countryShippingCreatedFlag);
		}
		return ($this->regionCreatedFlag && $this->countryCreatedFlag);
	}

	private function addRegionUpdater($config) {
		switch ($config['name']) {
			case 'region_id':
				$this->regionCreatedFlag = true;
				break;
			case 'country_id':
				$this->countryCreatedFlag = true;
				break;
			case 's_region_id':
				$this->regionShippingCreatedFlag = true;
				break;
			case 's_country_id':
				$this->countryShippingCreatedFlag = true;
				break;
		}
	}

	public function isCalendarCreated() {
		return $this->calendarLoadedFlag;
	}

	public function getCalendarConfig() {
		$html = '';
		if ($this->calendarLoadedFlag) {
			$calendar = Mage::app()->getLayout()->createBlock('core/html_calendar')->setTemplate('page/js/calendar.phtml');
			$html = $calendar->toHtml();
		}

		return $html;
	}

	public function isCountryRequireState($prefix = '') {
		if ($country = $this->getOptionValueByName($prefix . 'country_id')) {
			$regions = Mage::getModel('directory/country')->loadByCode($country)->getRegions();
			return (bool)$regions->getSize();
		}

		return null;
	}

	public function getCountryRegions() {
		$result = array();
		if ($country = $this->getOptionValueByName('country_id')) {
			$regions = Mage::getModel('directory/country')->loadByCode($country)->getRegions();
			if ($regions->getSize()) {
				foreach ($regions as $region) {
					$result[] = array(
						'value' => $region->getId(),
						'label' => $region->getName(),
					);
				}
			}
		}

		return $result;
	}

	/**
	 * @return Mage_Customer_Helper_Data
	 */
	public function getCustomerHelper() {
		return Mage::helper('customer');
	}

	public function getSuffixOptions() {
		if (method_exists($this->getCustomerHelper(), 'getNameSuffixOptions')) {
			return $this->getCustomerHelper()->getNameSuffixOptions();
		} else {
			return $this->getCustomerPrefixSuffixOptions('suffix_options');
		}
	}

	public function getPrefixOptions() {
		if (method_exists($this->getCustomerHelper(), 'getNamePrefixOptions')) {
			return $this->getCustomerHelper()->getNamePrefixOptions();
		} else {
			return $this->getCustomerPrefixSuffixOptions('prefix_options');
		}
	}

	public function getCustomerPrefixSuffixOptions($type, $store = null) {
		return $this->_prepareNamePrefixSuffixOptions(Mage::helper('customer/address')->getConfig($type, $store));
	}

	/**
	 * For older magento versions
	 *
	 * @param $options
	 * @return array|bool|string
	 */
	protected function _prepareNamePrefixSuffixOptions($options) {
		$options = trim($options);
		if (!$options) {
			return false;
		}
		$options = explode(';', $options);
		foreach ($options as &$v) {
			$v = $this->htmlEscape(trim($v));
		}
		return $options;
	}

	/**
	 * @return Itoris_RegFields_Helper_Mime
	 */
	private function getMimeHelper() {
		return Mage::helper('itoris_regfields/mime');
	}
}

?>