<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_CaptchaController extends Mage_Core_Controller_Front_Action {

	/**
	 * Get Securimage captcha image
	 */
	public function securimageAction() {
		require_once Mage::getBaseDir() . "/app/code/local/Itoris/RegFields/Helper/Captcha/securimage/securimage.php";
		$img = new Securimage();
		$img->show();
	}

	/**
	 * Get Alikonmod captcha image
	 */
	public function alikonAction() {
		require_once Mage::getBaseDir() . "/app/code/local/Itoris/RegFields/Helper/Captcha/alikon/captcha.php";
		$img = new alikoncaptcha();
		$img->image($img->captchacode());
	}

	/**
	 * Get CaptchaForm captcha image
	 */
	public function captchaFormAction() {
		require_once Mage::getBaseDir() . "/app/code/local/Itoris/RegFields/Helper/Captcha/captchaform/captchaform5.php";
		$img = new captchaform();
		$img->image($img->captchacode());
	}
}
 
?>