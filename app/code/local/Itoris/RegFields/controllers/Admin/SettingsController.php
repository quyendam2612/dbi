<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_Admin_SettingsController extends Itoris_RegFields_Controller_Admin_Controller {

	/**
	 * Settings page
	 */
	public function indexAction() {
		$this->_getSession()->setBeforUrl(Mage::helper('core/url')->getCurrentUrl());
		$websiteCode = $this->getRequest()->getParam('website');
		if (!empty($websiteCode)) {
			$website = Mage::app()->getWebsite($websiteCode);
			if (!Mage::helper('itoris_regfields')->isRegistered($website)) {
				$error = '<b style="color:red">'
						 . Mage::helper('itoris_regfields')->__('The extension is not registered for the website selected. Please register it with an additional S/N.')
						 . '</b>';
				Mage::getSingleton('adminhtml/session')->addError($error);
			}
		}
		$this->loadLayout();
		$settings = $this->getLayout()->createBlock( 'itoris_regfields/admin_config_settings' );
		$this->getLayout()->getBlock( 'content' )->append( $settings );
		$this->renderLayout();
	}

	/**
	 * Save settings action
	 */
	public function saveAction() {
		$websiteId = (int)$this->getRequest()->getParam('website_id');
		$storeId = (int)$this->getRequest()->getParam('store_id');
		if ($storeId) {
			$scope = 'store';
			$scopeId = (int)$storeId;
		} elseif ($websiteId) {
			$scope = 'website';
			$scopeId = $websiteId;
		} else {
			$scope = 'default';
			$scopeId = 0;
		}
		$data = $this->getRequest()->getPost();
		if (!isset($data['settings'])) {
			$this->_redirect('*/*');
			return;
		}
		$settings = $data['settings'];
		/** @var $model Itoris_RegFields_Model_Settings */
		$model = Mage::getModel('itoris_regfields/settings');

		$sections = $this->getRequest()->getParam('sections');

		try {
			$model->save($settings, $scope, $scopeId);
			if ($sections) {
				/** @var $formModel Itoris_RegFields_Model_Form */
				$formModel = Mage::getModel('itoris_regfields/form');
				$formModel->load($model->getViewId(), 'view_id');
				$formModel->setViewId($model->getViewId());
				$formModel->setConfig(serialize($sections));
				$formModel->save();
			}
		} catch (Exception $e) {
			$this->_getSession()->addError($e->getMessage());
			$this->_getSession()->addWarning('Settings have not been saved');
		}
		$this->_getSession()->addSuccess('Settings have been saved');

		$this->_redirectReferer($this->_getSession()->getBeforUrl());
	}

	protected function _isAllowed() {
		return Mage::getSingleton('admin/session')->isAllowed('admin/system/itoris_extensions/itoris_regfields');
	}
}
?>