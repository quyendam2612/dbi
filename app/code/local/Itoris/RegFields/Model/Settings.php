<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_RegFields_Model_Settings extends Varien_Object {

	/** @var Mage_Core_Model_Resource */
	private $resource;
	/** @var Varien_Db_Adapter_Pdo_Mysql */
	private $connection;
	private $tableView = 'itoris_regfields_view';
	private $tableSettings = 'itoris_regfields_settings';

	private $_scope;
	private $_scopeId;
	private $_settings;
	private $viewId = 0;

	const ENABLED = 1;
	const DISABLED = 2;

	public function __construct() {
		$this->resource = Mage::getSingleton('core/resource');
		$this->connection = $this->resource->getConnection('core_write');
		$this->tableView = $this->resource->getTableName($this->tableView);
		$this->tableSettings = $this->resource->getTableName($this->tableSettings);
	}

	public function save($settings, $scope = 'default', $scopeId = 0) {
			$this->_scope = $this->connection->quote($scope);
			$this->_scopeId = (int)$scopeId;
			$this->setViewId();
			$this->_deleteSettings();
			$newSettings = array();
			foreach($settings as $key => $value){
				$value = isset($value['value']) ? $value['value'] : 0;
				if(!(isset($settings[$key]['use_parent'])) || $scope == 'default'){
					$newSettings[$key] = array('value' => $value);
				}
			}
			if (!empty($newSettings)) {
				$this->_saveSettings($newSettings);
			}
			$this->_scope = null;
			$this->_scopeId = null;
	}

	public function load($websiteId, $storeId) {
		$websiteId = (int)$websiteId;
		$storeId = (int)$storeId;
		$this->setScope($websiteId, $storeId);
		$settings = $this->connection->fetchAll("
				SELECT e.scope, setting.value, setting.key
				FROM {$this->tableView} as e
				INNER JOIN {$this->tableSettings} as setting
					ON e.view_id = setting.view_id
				WHERE (e.scope = 'default' and e.scope_id = 0)
				OR (e.scope = 'website' and e.scope_id = $websiteId)
				OR (e.scope = 'store' and e.scope_id = $storeId)
		");
		$this->_saveSettingsIntoArray($settings);
		return $this;
	}

	private function _saveSettingsIntoArray($settings) {
		foreach($settings as $value){
			$this->_settings[$value['scope']][$value['key']] = $value['value'];
		}
	}

	public function __call($method, $args) {
        if (substr($method, 0, 3) == 'get') {
                $key = $this->_underscore(substr($method,3));
                if (isset($this->_settings['store'][$key])) {
					return $this->_settings['store'][$key];
				} elseif (isset($this->_settings['website'][$key])) {
					return $this->_settings['website'][$key];
				} elseif (isset($this->_settings['default'][$key])) {
					return $this->_settings['default'][$key];
				}
				return $this->getData($key, isset($args[0]) ? $args[0] : null);
        } else {
			parent::__call($method,$args);
		}
    }

	/**
	 * Check setting value is value of parent scope view
	 *
	 * @param $key
	 * @param bool $isStore
	 * @return bool
	 */
	public function isParentValue($key, $isStore = false) {
		if (isset($this->_settings['store'][$key])) {
			return false;
		}
		if (!$isStore) {
			if (isset($this->_settings['website'][$key])) {
				return false;
			}
		}
		return true;
	}

	private function _deleteSettings() {
		$this->connection->query("DELETE FROM {$this->tableSettings} WHERE view_id = {$this->viewId}");
	}

	private function _saveSettings($settings) {
		$settingsValues = array();
		foreach($settings as $key => $values){
			$value = (int)$values['value'];
			$settingsValues[] =  "($this->viewId, " . $this->connection->quote($key) . ", $value)";
		}
		$settingsValues = implode(',', $settingsValues);
		$this->connection->query("INSERT INTO {$this->tableSettings} (`view_id`, `key`, `value`) VALUES $settingsValues");
	}

	/**
	 * Save view id for current scope if it doesn't exist in db. Return the view id.
	 *
	 * @return int
	 */
	private function setViewId() {
		$this->viewId = (int)$this->connection->fetchOne("select view_id from {$this->tableView} where scope = {$this->_scope} and scope_id = {$this->_scopeId}");
		if (!$this->viewId) {
			$this->connection->query("insert into {$this->tableView} (scope, scope_id) values ({$this->_scope}, {$this->_scopeId})");
			$this->setViewId();
		}
		return $this->viewId;
	}

	public function getDefaultData() {
		return array(
				'enable' => $this->getEnable(),
		);
	}

	public function setScope($websiteId, $storeId) {
		if ($storeId) {
			$this->_scope = $this->connection->quote('store');
			$this->_scopeId = $storeId;
		} elseif ($websiteId) {
			$this->_scope = $this->connection->quote('website');
			$this->_scopeId = $websiteId;
		} else {
			$this->_scope = $this->connection->quote('default');
			$this->_scopeId = 0;
		}
	}

	public function getViewId() {
		if (!$this->viewId) {
			$this->setViewId();
		}
		return $this->viewId;
	}

	/**
	 * Get current view id
	 *
	 * @param int $websiteId
	 * @param int $storeId
	 * @return int
	 */
	public function getActiveViewId($websiteId = 0, $storeId = 0) {
		if ($this->isFormActiveForScope('store')) {
			$scope = $this->connection->quote('store');
			$storeId = (int)$storeId;
			return (int)$this->connection->fetchOne("select view_id from {$this->tableView} where scope = {$scope} and scope_id = {$storeId}");
		} elseif ($this->isFormActiveForScope('website')) {
			$scope = $this->connection->quote('website');
			$websiteId = (int)$websiteId;
			return (int)$this->connection->fetchOne("select view_id from {$this->tableView} where scope = {$scope} and scope_id = {$websiteId}");
		} else {
			$scope = $this->connection->quote('default');
			return (int)$this->connection->fetchOne("select view_id from {$this->tableView} where scope = {$scope} and scope_id = 0");
		}
	}

	private function isFormActiveForScope($scope) {
		return isset($this->_settings[$scope]['form_active']) && $this->_settings[$scope]['form_active'];
	}

}
?>