<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_REGFIELDS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

$this->startSetup();

$this->run("

	CREATE TABLE IF NOT EXISTS {$this->getTable('itoris_regfields_view')} (
		`view_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`scope` ENUM('default', 'website', 'store') NOT NULL ,
		`scope_id` INT UNSIGNED NOT NULL ,
		UNIQUE(`scope`, `scope_id`)
	) ENGINE = InnoDB DEFAULT CHARSET = utf8;

	CREATE TABLE IF NOT EXISTS {$this->getTable('itoris_regfields_settings')} (
		`setting_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`view_id` INT UNSIGNED NOT NULL,
		`key` VARCHAR( 255 ) NOT NULL ,
		`value` INT UNSIGNED NOT NULL ,
		UNIQUE(`view_id`, `key`),
		FOREIGN KEY (`view_id`) REFERENCES {$this->getTable('itoris_regfields_view')} (`view_id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB DEFAULT CHARSET=utf8;

	CREATE TABLE IF NOT EXISTS {$this->getTable('itoris_regfields_form')} (
		`form_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`view_id` INT UNSIGNED NOT NULL,
		`config` TEXT NOT NULL,
		UNIQUE (`view_id`),
		FOREIGN KEY (`view_id`) REFERENCES {$this->getTable('itoris_regfields_view')} (`view_id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB DEFAULT CHARSET=utf8;

	CREATE TABLE IF NOT EXISTS {$this->getTable('itoris_regfields_customer_options')} (
		 `option_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
		 `customer_id` int(10) unsigned NOT NULL,
		 `key` varchar(255) not null,
		 `value` text not null,
		 unique(`customer_id`, `key`),
		 foreign key (`customer_id`) references {$this->getTable('customer_entity')} (`entity_id`) on delete cascade on update cascade
	) ENGINE = InnoDB DEFAULT CHARSET=utf8;

");

$this->endSetup();
?>