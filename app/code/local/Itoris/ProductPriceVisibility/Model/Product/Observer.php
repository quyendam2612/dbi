<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Model_Product_Observer {

	protected $visibilityProductCollection = null;
	protected $visibilityPriceCollection = null;
	protected $globalSettings = null;
    protected $_productGroupId = array();
    protected $_priceGroupId = array();

    protected function getVisibilityProductCollection() {
		if (is_null($this->visibilityProductCollection)) {
			return $this->visibilityProductCollection = Mage::getModel('itoris_productpricevisibility/settings');
		} else {
			return $this->visibilityProductCollection;
		}
	}

    protected function productGroupId($productId, $storeId) {
		if (!array_key_exists($productId, $this->_productGroupId)) {
            $this->_productGroupId[$productId] = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_product_visibility_group', $productId, $storeId);
		}
	    return $this->_productGroupId[$productId];

	}

    protected function priceGroupId($productId, $storeId) {
		if (!array_key_exists($productId, $this->_priceGroupId)) {
            $this->_priceGroupId[$productId] = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_price_visibility_group', $productId, $storeId);
		}
	    return $this->_priceGroupId[$productId];

	}

	public function changePrice($obj) {
		$block = $obj->getBlock();
		if($this->getDataHelper()->isRegisteredFrontend()) {
			if ($block instanceof Mage_Catalog_Block_Product_Price) {
				$product = $block->getProduct();
				$checkStore = (int)Mage::app()->getStore()->getId();
				$productCollection = $this->getVisibilityProductCollection();
				$productModel = $productCollection->load(0, $checkStore, $product->getId());
				$productGroupId = $this->productGroupId($product->getId(), $checkStore);
				$priceGroupId = $this->priceGroupId($product->getId(), $checkStore);
                $this->globalSettings = $productCollection->load(0, $checkStore);
                $globalGroupId = $this->globalSettings->getGlobalUserGroups();
                $globalAllHide = ($this->getDataHelper()->customerGroup($globalGroupId)
                    && $this->getDataHelper()->isVisibleByRestrictionDate($this->globalSettings->getGlobalRestrictionBegin(), $this->globalSettings->getGlobalRestrictionEnd())
                );
                if ($this->getDataHelper()->isRightConditions($productModel, $productGroupId) || $this->getDataHelper()->isRightConditions($productModel, $priceGroupId, 'price')) {
					$this->productVisibility($product, $obj, $productModel);
                    return;
				} elseif ($globalAllHide &&
                    ($this->globalSettings->getGlobalHidingMode() != Itoris_ProductPriceVisibility_Model_Settings::HIDE_PRODUCT_GLOBAL
                        || $this->globalSettings->getGlobalHidingMode() != Itoris_ProductPriceVisibility_Model_Settings::HIDE_ALL_GLOBAL)
                ) {
                    if (($this->getDataHelper()->rulesForCategory($product->getCategoryIds(), $checkStore, 'out_of_stock')
                            || $this->getDataHelper()->rulesForCategory($product->getCategoryIds(), $checkStore, 'price'))
                        && !($this->getDataHelper()->isRightConditions($productModel, $productGroupId) || $this->getDataHelper()->isRightConditions($productModel, $priceGroupId, 'price'))
                    ) {
                        $this->categoryVisibility($product, $obj);
                    } else {
                        $this->productVisibility($product, $obj, $productModel);
                    }
                } else {
					$this->categoryVisibility($product, $obj);
				}

			}
		}
	}

	protected function productVisibility($product, $obj, $productModel) {
		$checkStore = (int)Mage::app()->getStore()->getId();
		if (!is_null($productModel)) {
			$restrictionBegin = $productModel->getProductRestrictionBegin();
			$restrictionEnd = $productModel->getProductRestrictionEnd();
			if (!empty($restrictionBegin) || !empty($restrictionEnd)) {
				$productGroupId = $this->productGroupId($product->getId(), $checkStore);
				if ($this->getDataHelper()->isRightConditions($productModel, $productGroupId)){
					if ($productModel->getProductHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_NO_PRICE) {
						$transport = $obj->getTransport();
						$priceGroupId = $this->priceGroupId($product->getId(), $checkStore);
						if ($this->getDataHelper()->isRightConditions($productModel, $priceGroupId, 'price')
							&& $productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_SHOW_OUT_OF_STOCK
						) {
							$transport->setHtml('<div></div>');
						} else {
							$transport->setHtml('<div class="itoris_product_visibility"></div>');
						}
						$this->removeAddToCart($product);
					} elseif ($productModel->getProductHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_SHOW_OUT_OF_STOCK) {
						$this->priceVisibilityRules($productModel, $product, $obj, true);
					}
				} else {
					$this->priceVisibilityRules($productModel, $product, $obj);
				}
			} else {
				$this->priceVisibilityRules($productModel, $product, $obj);
			}
		}
	}

	protected function priceVisibilityRules($productModel, $product, $obj, $productOutOfStock = false) {
		$checkStore = (int)Mage::app()->getStore()->getId();
		if (!is_null($productModel)) {
			$priceGroupId = $this->priceGroupId($product->getId(), $checkStore);
            $globalGroupId = $this->globalSettings->getGlobalUserGroups();
            $global = ($this->getDataHelper()->customerGroup($globalGroupId)
                && $this->getDataHelper()->isVisibleByRestrictionDate($this->globalSettings->getGlobalRestrictionBegin(), $this->globalSettings->getGlobalRestrictionEnd())
            );
            $transport = $obj->getTransport();
            $getHtml = $transport->getHtml();
			if ($this->getDataHelper()->isRightConditions($productModel, $priceGroupId, 'price')) {
				if ($productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_NO_PRICE) {
					if ($productOutOfStock) {
						$transport->setHtml('<div></div>');
					} else {
						$transport->setHtml('<div class="itoris_product_visibility"></div>');
					}
					$this->removeAddToCart($product);
				} elseif ($productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_CUSTOM_MESSAGE) {
					if (strlen($getHtml) > 1) {
						if ($productOutOfStock) {
							$transport->setHtml('<div>' . $productModel->getPriceHidingMessage() . '</div>');
						} else {
							$transport->setHtml('<div class="itoris_product_visibility">' . $productModel->getPriceHidingMessage() . '</div>');
						}
						$this->removeAddToCart($product);
					}
				} elseif ($productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRICE_SHOW_OUT_OF_STOCK) {
					$transport->setHtml('<div class="itoris_product_visibility_price_out_of_stock"></div>');
					$this->removeAddToCart($product);
				} elseif ($productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_PRICE_DISALLOW_ADD_TO_CART) {
					if (strlen($getHtml) > 1) {
						$transport->setHtml('<div class="itoris_product_visibility">' . $getHtml . '</div>
							<div class="itoris_product_visibility_restriction_message">' . $productModel->getPriceRestrictionMessage() . '</div>');
						$this->removeAddToCart($product);
					}
				}
			} else {
                if ($global) {
                    if ($this->globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_PRICES_GLOBAL) {
                        if ($productOutOfStock) {
                            $transport->setHtml('<div></div>');
                        } else {
                            $transport->setHtml('<div class="itoris_product_visibility"></div>');
                        }
                        $this->removeAddToCart($product);
                    } elseif($this->globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::SHOW_OUT_OF_STOCK_GLOBAL) {
                        $transport->setHtml('<div class="itoris_product_visibility_price_out_of_stock"></div>');
                        $this->removeAddToCart($product);
                    } elseif($this->globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::SHOW_CUSTOM_STOCK_GLOBAL) {
                        if (strlen($getHtml) > 1) {
                            if ($productOutOfStock) {
                                $transport->setHtml('<div>' . $this->globalSettings->getGlobalCustomStockStatus() . '</div>');
                            } else {
                                $transport->setHtml('<div class="itoris_product_visibility">' . $this->globalSettings->getGlobalCustomStockStatus() . '</div>');
                            }
                            $this->removeAddToCart($product);
                        }
                    } elseif($this->globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::SHOW_PRICE_DISALLOW_ADD_TO_CART_GLOBAL) {
                        if (strlen($getHtml) > 1) {
                            $transport->setHtml('<div class="itoris_product_visibility">' . $getHtml . '</div>
							<div class="itoris_product_visibility_restriction_message">' . $this->globalSettings->getGlobalRestrictionMessage() . '</div>');
                            $this->removeAddToCart($product);
                        }
                    }
                }
            }
		}
	}

	protected function removeAddToCart($product) {
		$product->setIsSalable(false);
	}

	protected function categoryVisibility($product, $obj) {
		$categoryIds = $product->getCategoryIds();
		$checkStore = (int)Mage::app()->getStore()->getId();
		$categoryCollection = $this->getVisibilityProductCollection();
		foreach ($categoryIds as $categoryId) {
			$categoryVisibilityModel = $categoryCollection->load(0, $checkStore, 0, $categoryId);
			$groupId = $this->getDataHelper()->getGroupIdForCategory('itoris_productpricevisibility_category_visibility_group', $categoryId, $checkStore);
			$mode = (int)$categoryVisibilityModel->getCategoryHidingMode();
			$transport = $obj->getTransport();
			$getHtml = $transport->getHtml();
			if ($this->getDataHelper()->isRightConditions($categoryVisibilityModel, $groupId, 'category')) {
				if ($mode == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::NO_PRICE) {
					$transport->setHtml('<div class="itoris_product_visibility"></div>');
					$this->removeAddToCart($product);
				} elseif ($mode == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::CUSTOM_MESSAGE) {
					if (strlen($getHtml) > 1) {
						$transport->setHtml('<div class="itoris_product_visibility" style="overflow: hidden;">' . $categoryVisibilityModel->getCategoryHidingMessage() . '</div>');
						$this->removeAddToCart($product);
					}
				} elseif ($mode == Itoris_ProductPriceVisibility_Block_Admin_Category_Edit_Tab_VisibilityRules::SHOW_PRICE_DISALLOW_ADD_TO_CART) {
					if (strlen($getHtml) > 1) {
						$transport->setHtml('<div class="itoris_product_visibility">' . $getHtml . '</div>
							<div class="itoris_product_visibility_restriction_message">' . $categoryVisibilityModel->getCategoryRestrictionMessage() . '</div>');
						$this->removeAddToCart($product);
					}
				}
			}
		}
	}



	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */

	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility/data');
	}
}
?>
