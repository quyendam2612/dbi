<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Model_Wishlist_PrepareCollection_Observer {

	public function beforeLoadWishlist($obj) {
		$collection = $obj->getCollection();
		if ($collection instanceof Mage_Wishlist_Model_Resource_Item_Collection || $collection instanceof Mage_Wishlist_Model_Mysql4_Item_Collection) {
			$isAdmin = Mage::app()->getStore()->isAdmin();
				if (!$isAdmin && $this->getDataHelper()->isRegisteredFrontend()) {
					$checkStore = (int)Mage::app()->getStore()->getId();
					$productVisibilityModel = Mage::getModel('itoris_productpricevisibility/settings');
					$settingsByProductId = $productVisibilityModel->loadSettingsForProduct(0, $checkStore);
					$productIdHide = array();
					$resource = Mage::getSingleton('core/resource');
					$connection = $resource->getConnection('read');
					$categoryTable = $resource->getTableName('catalog_category_product_index');
					$query = $collection->getSelectSql(true);
					$productIds = $connection->fetchAll($query);
                    $idProductVisible = array();
					foreach ($productIds as $idForLoad) {
						$categoryIds = array();
						$productId = $idForLoad['product_id'];
						$settings = $productVisibilityModel->load(0, $checkStore, $productId);
						$categoryIdsFromTable = $connection->fetchAll("select category_id from {$categoryTable} where product_id = {$productId} and is_parent = 1");
						foreach ($categoryIdsFromTable as $ids) {
							$categoryIds[] = $ids['category_id'];
						}
						if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'hide')) {
							$priceGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_price_visibility_group', $productId, $checkStore);
							if (!$this->getDataHelper()->customerGroup($priceGroupId)
								&& $this->getDataHelper()->isVisibleByRestrictionDate($settings->getPriceRestrictionBegin(), $settings->getPriceRestrictionEnd())
							) {
								$productIdHide[] = $productId;
							}
						} else if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'out_of_stock') || $this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'price')) {
                            $idProductVisible[] = $productId;
                        }
					}
					foreach ($settingsByProductId as $productId => $product) {
						$product = new Varien_Object($product);
						$productGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_product_visibility_group', $productId, $checkStore);
						if ($product->getData('product_hiding_mode') == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::PRODUCT_HIDE_COMPLETELY
							&& $this->getDataHelper()->customerGroup($productGroupId)
							&& $this->getDataHelper()->isVisibleByRestrictionDate($product->getData('product_restriction_begin'), $product->getData('product_restriction_end'))
						) {
							$productIdHide[] = $productId;
						} elseif ($this->getDataHelper()->customerGroup($productGroupId)
                            && $this->getDataHelper()->isVisibleByRestrictionDate($product->getData('product_restriction_begin'), $product->getData('product_restriction_end'))) {
                            $idProductVisible[] = $productId;
                        }
					}
                    $globalSettings = $productVisibilityModel->load(0, $checkStore);
                    $globalGroupId = $globalSettings->getGlobalUserGroups();
                    $globalAllHide = (
                        ($globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_ALL_GLOBAL
                            || $globalSettings->getGlobalHidingMode() == Itoris_ProductPriceVisibility_Model_Settings::HIDE_PRODUCT_GLOBAL
                        )
                        && $this->getDataHelper()->customerGroup($globalGroupId)
                        && $this->getDataHelper()->isVisibleByRestrictionDate($globalSettings->getGlobalRestrictionBegin(), $globalSettings->getGlobalRestrictionEnd())
                    );

                    if ($globalAllHide) {
                        $wishlistProducts = $collection->getData();
                        if (!empty($idProductVisible)) {
                            foreach ($wishlistProducts as $key =>  $data) {
                                $productIdHide[] = (int)$data['product_id'];
                                foreach ($idProductVisible as $id) {
                                    if ($data['product_id'] == $id) {
                                        unset($productIdHide[$key]);
                                    }
                                }
                            }
                        }
                    }
					if (!empty($productIdHide)) {
						$collection->addFieldToFilter('product_id', array('nin' => $productIdHide));
					}
				}
		}
	}

	/**
	 * @return Itoris_ProductPriceVisibility_Helper_Data
	 */
	public function getDataHelper() {
		return Mage::helper('itoris_productpricevisibility');
	}
}
?>