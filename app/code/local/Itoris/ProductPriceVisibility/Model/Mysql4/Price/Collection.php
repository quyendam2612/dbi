<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

  

class Itoris_ProductPriceVisibility_Model_Mysql4_Price_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	protected $groupTable = 'itoris_productpricevisibility_price_visibility_group';

    protected function _construct() {
        $this->_init('itoris_productpricevisibility/price');
		//$this->groupTable = $this->getTable('price_group');
    }

	/*protected function _initSelect() {
		$this->getSelect()->reset(null)->from(array('main_table' => $this->getMainTable()))
			->joinLeft(
					array('groups' => $this->groupTable),
					'groups.product_id = main_table.product_id',
					array('group_id' => 'group_concat(distinct groups.group_id)')
			)->group('main_table.product_id');

		return $this;
	}

	public function addGroupFilter($groupId) {
		$this->_select->having("group_id IS NULL OR FIND_IN_SET('" . intval($groupId) . "', group_id)");
		return $this;
	}*/
}
?>