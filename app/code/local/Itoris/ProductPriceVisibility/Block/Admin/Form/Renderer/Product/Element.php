<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTPRICEVISIBILITY
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductPriceVisibility_Block_Admin_Form_Renderer_Product_Element extends Mage_Adminhtml_Block_Catalog_Form_Renderer_Fieldset_Element {

	protected function _construct() {
		$this->setTemplate('itoris/productpricevisibility/form/renderer/product/element.phtml');
	}

	public function canDisplayUseDefault() {
		$checkStore = Mage::app()->getRequest()->getParam('store');
		if ($checkStore) {
			return true;
		} else {
			return false;
		}
	}

	public function getScopeLabel() {
		$html = '';
		/*$attribute = $this->getElement()->getEntityAttribute();
		if (!$attribute || Mage::app()->isSingleStoreMode() || $attribute->getFrontendInput()=='gallery') {
			return $html;
		}
		if ($attribute->isScopeGlobal()) {
			$html .= Mage::helper('adminhtml')->__('[GLOBAL]');
		} elseif ($attribute->isScopeWebsite()) {
			$html .= Mage::helper('adminhtml')->__('[WEBSITE]');
		} elseif ($attribute->isScopeStore()) {
			$html .= Mage::helper('adminhtml')->__('[STORE VIEW]');
		}*/

		return $html;
	}

	public function usedDefault() {
		return $this->getElement()->getShowCheckbox();
	}
}
?>