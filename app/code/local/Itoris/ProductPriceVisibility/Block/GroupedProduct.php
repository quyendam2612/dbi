<?php
    /**
     * ITORIS
     *
     * NOTICE OF LICENSE
     *
     * This source file is subject to the ITORIS's Magento Extensions License Agreement
     * which is available through the world-wide-web at this URL:
     * http://www.itoris.com/magento-extensions-license.html
     * If you did not receive a copy of the license and are unable to
     * obtain it through the world-wide-web, please send an email
     * to sales@itoris.com so we can send you a copy immediately.
     *
     * DISCLAIMER
     *
     * Do not edit or add to this file if you wish to upgrade the extensions to newer
     * versions in the future. If you wish to customize the extension for your
     * needs please refer to the license agreement or contact sales@itoris.com for more information.
     *
     * @category   ITORIS
     * @package    ITORIS_PRODUCTPRICEVISIBILITY
     * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
     * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
     */



class Itoris_ProductPriceVisibility_Block_GroupedProduct extends Mage_Core_Block_Template {

    protected function _construct() {
        if ($this->getDataHelper()->isRegisteredFrontend()) {
            $checkStore = (int)Mage::app()->getStore()->getId();
            $productVisibilityCollection = Mage::getModel('itoris_productpricevisibility/settings');
            $product = Mage::registry('current_product');
            if ($product->getTypeId() == 'grouped') {
                $currentProductId = $product->getId();
                $categoryIds = $product->getCategoryIds();
                if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'out_of_stock')) {
                    $product->setIsSalable(false);
                }
                $productModel = $productVisibilityCollection->load(0, $checkStore, $currentProductId);
                $priceGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_price_visibility_group', $currentProductId, $checkStore);
                if ($this->getDataHelper()->isRightConditions($productModel, $priceGroupId, 'price')) {
                    if ($productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_CUSTOM_MESSAGE
                        || $productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_PRICE_DISALLOW_ADD_TO_CART
                    ) {
                        $product->setIsSalable(false);
                        $this->setTemplate('itoris/productpricevisibility/grouped.phtml');
                    }
                }
             }
        }
    }

    public function getMessages() {
        if ($this->getDataHelper()->isRegisteredFrontend()) {
            $checkStore = (int)Mage::app()->getStore()->getId();
            $productVisibilityCollection = Mage::getModel('itoris_productpricevisibility/settings');
            $product = Mage::registry('current_product');
            if ($product->getTypeId() == 'grouped') {
                $currentProductId = $product->getId();
                $categoryIds = $product->getCategoryIds();
                if ($this->getDataHelper()->rulesForCategory($categoryIds, $checkStore, 'out_of_stock')) {
                    $product->setIsSalable(false);
                }
                $productModel = $productVisibilityCollection->load(0, $checkStore, $currentProductId);
                $priceGroupId = $this->getDataHelper()->getGroupId('itoris_productpricevisibility_price_visibility_group', $currentProductId, $checkStore);
                if ($this->getDataHelper()->isRightConditions($productModel, $priceGroupId, 'price')) {
                    if ($productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_CUSTOM_MESSAGE) {
                        return $productModel->getPriceHidingMessage();
                    } elseif($productModel->getPriceHidingMode() == Itoris_ProductPriceVisibility_Block_Admin_Product_Edit_Tab_PriceVisibility::SHOW_PRICE_DISALLOW_ADD_TO_CART) {
                        return $productModel->getPriceRestrictionMessage();
                    }
                }
            }
        }
    }

    /**
     * @return Itoris_ProductPriceVisibility_Helper_Data
     */

    public function getDataHelper() {
        return Mage::helper('itoris_productpricevisibility');
    }

}
?>