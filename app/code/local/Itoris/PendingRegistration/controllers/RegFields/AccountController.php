<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


require_once Mage::getModuleDir('controllers', 'Itoris_RegFields') . DS . 'AccountController.php';

class Itoris_PendingRegistration_RegFields_AccountController extends Itoris_RegFields_AccountController {

	protected $disableEmailFlag = false;

	public function confirmAction() {
		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper('itoris_pendingregistration/data');
		$helper->init(null);
		$scope = $helper->getCustomerScope($this->_getSession()->getCustomer());
		if ($helper->isRegisteredAutonomous()
			&& Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($scope)
			&& $helper->isCanSendEmail(Itoris_PendingRegistration_Model_Template::$EMAIL_APPROVED, $scope)
		) {
			$this->disableEmailFlag = true;
		}
		parent::confirmAction();
	}

	protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false) {
		if ($this->disableEmailFlag) {
			$this->_getSession()->addSuccess(
				$this->__('Thank you for registering with %s.', Mage::app()->getStore()->getFrontendName())
			);
			$successUrl = Mage::getUrl('*/*/index', array('_secure'=>true));
			if ($this->_getSession()->getBeforeAuthUrl()) {
				$successUrl = $this->_getSession()->getBeforeAuthUrl(true);
			}
		} else {
			$successUrl = parent::_welcomeCustomer($customer, $isJustConfirmed);
		}

		return $successUrl;
	}
}
?>