<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Block_Admin_Dropdown extends Mage_Core_Block_Template//Mage_Adminhtml_Block_Widget_Button
{
	protected function _construct(){
		$this->setTemplate('itoris/pendingregistration/dropdown.phtml');
	}

	public function getEngineToggleUrl(){
		return 'changeScope Url';
	}

	public function getActivateUrl(){
		return $this->getToggleUrl('activate');
	}

	public function getDeactivateUrl(){
		return $this->getToggleUrl('deactivate');
	}

	private function getToggleUrl($action){
		/** @var $hurl Mage_Core_Helper_Url */
		$hurl = Mage::helper('core/url');
		/** @var $url Mage_Adminhtml_Model_Url */
		$url = Mage::getModel('adminhtml/url');
		return $url->getUrl('*/*/engine', array_merge( $this->getScope()->getSummaryForUrl(),
										array(
											'action' => $action,
											'back_url' => $hurl->getCurrentBase64Url()
										)));
	}

	public function isActive(){
		return Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($this->getScope());
	}

	public function setScope(Itoris_PendingRegistration_Model_Scope $scope){
		return $this->setData('scope', $scope);
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Scope
	 */
	public function getScope(){
		return $this->getData('scope');
	}

}

?>