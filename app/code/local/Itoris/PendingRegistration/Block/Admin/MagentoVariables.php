<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Block_Admin_MagentoVariables extends Mage_Core_Block_Text{
	protected function _construct(){
		$variables = array();
		$variables[] = array( 'label'=>$this->__('Registration information'), 'value' => array(
			array(
			'label' => $this->__('Account create time'),
			'value'	=> '{{ipr_customer_create_time}}'
			),
			array(
				'label' => $this->__('Customer IP'),
				'value'	=> '{{ipr_customer_ip}}'
			),
			array(
				'label' => $this->__('Customer First name'),
				'value'	=> '{{ipr_customer_first_name}}'
			),
			array(
				'label' => $this->__('Customer Last name'),
				'value'	=> '{{ipr_customer_last_name}}'
			),
			array(
				'label' => $this->__('Customer Email'),
				'value'	=> '{{ipr_customer_email}}'
			)
		));
		$jsPath = Mage::getBaseUrl('js');
		$this->setText('<link rel="stylesheet" type="text/css" href="'.$jsPath.'/extjs/resources/css/ext-all.css">
	     <script type="text/javascript">
	      Variables.init(null, "MagentovariablePlugin.insertVariable");
	      MagentovariablePlugin.variables = '.Zend_Json::encode(
	       $variables ).'
	     </script>
	     <style>
	     	.icon-head{
	     		background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAY9JREFUeNpi/P//PwM1ABMDlQDLm3hGqjiJBcYQ6b9JlgFvCtURBnH5loAFRGa/JM2QVHGw3m+beyBhxOVdwMDlUwSWYPj7mygMNgSoB6QX4bVfPxi43LKACv4wvMmQYRCZdAe/S/JUGLg88yB6gHrhBv19douBWUiagcsxmYHhzy+wQpGui9gNKdNn4HLNgKj98RUtsEEC3z6BmVzW0QwMv3+CNYg0HEY1pMGWgcshEaIGqh41HX0FCn75AMb/P7xi+HZgPgOXVSRYI0wcbAhQDCT37+U9uDgYw130FchhYmH4DwzEt3NSGLiMAxi4dD0YGH5+Z3jT4wtxKZLYu+nxDEKxkxiYuPhRvcYMzCb/3zxieLu2loFLx42BS82GgeHTawj98xvEIDSxd4vzGISCGhiY2HmQXPT+JcPbHd0MXCpWDFwy+mA+DID5UDUoYj++MLxb18Ag5JQNFmN8HccAziJc0voITUSCb08uMnx7ehE1i3DxSQOd/oYkg0B6MAx6c30bRZmWcdCVRwABBgBqPrYxsOEtAQAAAABJRU5ErkJggg==);
	     	}
	     </style>
	    ');
	}
}
?>