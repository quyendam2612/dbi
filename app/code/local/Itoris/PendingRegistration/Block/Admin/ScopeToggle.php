<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */
/**
 * User: Vadim
 * Date: 26.05.11
 * Time: 14:47
 */
abstract class Itoris_PendingRegistration_Block_Admin_ScopeToggle extends Mage_Adminhtml_Block_Template{
	protected function _construct(){
		$this->_initTemplate();
		$this->setActive(false);
	}

	protected abstract function _initTemplate();

	public function isHasDefaultScope(){
		$result = $this->getScope()->getStoreId() === null
				&& $this->getScope()->getWebsiteId() === null;

		return $result;
	}

	public function isParentIsDefault(){
		return $this->getScope()->getStoreId() === null;
	}

	/**
	 * @return Itoris_PendingRegistration_Helper_Data
	 */
	public function getDataHelper(){
		return Mage::helper('itoris_pendingregistration');
	}

	public function getToggleScopeUrl(){
		$params = $this->getScope()->getSummaryForUrl();
		$params["action"] = $this->isActive() ? 'deactivate' : 'activate';
		/** @var $hurl Mage_Core_Helper_Url */
		$hurl = Mage::helper("core/url");
		$params["back_url"] = $hurl->getCurrentBase64Url();
		return $this->getUrl('itoris_pendingregistration/index/toggleEngineScope', $params);
	}

	public function setScope(Itoris_PendingRegistration_Model_Scope $scope){
		return $this->setData('scope', $scope);
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Scope
	 */
	public function getScope(){
		return $this->getData('scope');
	}

	public function isActive(){
		return $this->getData('is_active');
	}

	public function setActive($active){
		$this->setData('is_active', $active);
	}
}
?>