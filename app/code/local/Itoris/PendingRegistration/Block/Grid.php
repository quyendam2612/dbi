<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Block_Grid extends Mage_Adminhtml_Block_Customer_Grid
{

	protected function _prepareColumns() {

		if (method_exists('Mage','getVersionInfo')) {
			$this->addColumnsOrder( 'status', 'email' );
		}

		$this->addColumn('status', array(
			'header'    => $this->__('Status'),
			'width'     => '150',
			'index'     => 'status',
			'type'	=> 'options',
			'filter_condition_callback' => array($this, 'statusFilterCondition'),
			'options'	=> array(
				$this->__('Pending'),
				$this->__('Approved'),
				$this->__('Declined'),
			)
		));
		return parent::_prepareColumns();
	}

	/**
	 * @param $collection Mage_Customer_Model_Resource_Customer_Collection
	 * @param $column Mage_Adminhtml_Block_Widget_Grid_Column
	 */
	protected function statusFilterCondition($collection, $column) {
		$collection->getSelect()->where('status = ?', $column->getFilter()->getValue());
	}

	public function setCollection($collection) {
		// leave it empty to prevent parent from saving its own collection
	}
	
	protected function _prepareCollection() {
		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper('itoris_pendingregistration');
		$helper->init(null);
		/** @var $resource Mage_Core_Model_Resource */
		$resource = Mage::getSingleton('core/resource');
		$db = $resource->getConnection('core_write');
		$usersTableName = $resource->getTableName('itoris_pendingregistration_users');
		$customerTableName = $resource->getTableName('customer_entity');
		$db->query( 'DELETE FROM '.$usersTableName.' WHERE customer_id=0' );
		$result = $db->query( 'SELECT entity_id FROM '.$customerTableName );
		$customers = $result->fetchAll();
		$cCnt = count($customers);
		for($i=0; $i<$cCnt; $i++) {
			$cid = intval( $customers[ $i ][ 'entity_id' ] );
			$result = $db->query( 'SELECT COUNT(*) FROM '.$usersTableName.' WHERE customer_id='.$cid );
			if (!$result->fetchColumn( 0 )) {
				$db->query( 'INSERT INTO '.$usersTableName.' SET customer_id='.$cid.', status=1' );
			}
		}
	    
		$collection = Mage::getResourceModel('customer/customer_collection')
			->addNameToSelect()
			->addAttributeToSelect('email')
			->addAttributeToSelect('created_at')
			->addAttributeToSelect('group_id')
			->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
			->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
			->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
			->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
			->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left')
			->joinTable( $usersTableName, 'customer_id=entity_id', array('status'), null );

		$this->_collection = $collection;

		return parent::_prepareCollection();
	}
}

?>