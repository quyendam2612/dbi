<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Helper_Data extends Mage_Core_Helper_Abstract 
{
	private $request;
	
	private $componentAlias = 'pending_registration';
	
	public $templatesTableName = 'itoris_pendingregistration_templates';
	public $usersTableName = 'itoris_pendingregistration_users'; 
	public $settingsTableName = 'itoris_pendingregistration_settings';

	public function __construct() {
		// init table names
		/** @var $db Mage_Core_Model_Resource */
		$db = Mage::getSingleton('core/resource');
		$this->templatesTableName = $db->getTableName($this->templatesTableName);
		$this->usersTableName = $db->getTableName($this->usersTableName);
		$this->settingsTableName = $db->getTableName($this->settingsTableName);
	}

	public function init($request) {
		// save request, necessary for check registration
		$this->request = $request;

		// init constants
		if (!defined('IPR_EMAIL_REG_TO_ADMIN')) {
			define('IPR_EMAIL_REG_TO_ADMIN', 1);
			define('IPR_EMAIL_REG_TO_USER', 2);
			define('IPR_EMAIL_APPROVED', 3);
			define('IPR_EMAIL_DECLAINED', 4);
		}

		$this->checkConfiguration();
	}
	
	public function customerLogout()
	{
		Mage::getSingleton('customer/session')->logout()->setBeforeAuthUrl(Mage::getUrl('*/*/login'));
		Mage::app()->getResponse()->setRedirect(Mage::getUrl('*/*/login'));
		Mage::app()->getResponse()->sendHeaders();
		die;
	}
	
	public function isCanSendEmail( $templateType, Itoris_PendingRegistration_Model_Scope $scope )
	{
		/** @var $template Itoris_PendingRegistration_Model_Template */
		$template = Mage::getModel( 'itoris_pendingregistration/template' )->load( $templateType, 'type', $scope );
		return $template->isActive() && $template->getEmailContent()!='' && $template->getFromName()!='' && $template->getFromEmail()!='' && $template->getSubject()!='';
	}
	
	public function sendEmail($templateType, $user, Itoris_PendingRegistration_Model_Scope $scope) {
		if (!$this->isCanSendEmail( $templateType, $scope )) {
			return;
		}

		/** @var $template Itoris_PendingRegistration_Model_Template */
		$template = Mage::getModel( 'itoris_pendingregistration/template' )->load( $templateType, 'type', $scope );

		/** @var $customer Mage_Customer_Model_Customer */
		$customer = Mage::getModel( 'itoris_pendingregistration/users' )->load( $user->getEntityId() );

		/** @var $mailer Mage_Core_Model_Email_Template */
		$mailer = Mage::getModel( 'core/email_template' );
		$mailer->setSenderEmail( $template->getFromEmail() );
		$mailer->setSenderName( $template->getFromName() );
		
		
		$rawCC = $template->getCc();
		$cc = '';
		if( !empty($rawCC) )
		{
			if( strpos($rawCC, ',') !== FALSE )	
			{
				$cc = explode( ',', $rawCC );
				$ccCnt = count($cc);
				for( $i=0; $i<$ccCnt; $i++ )
				{
					$cc[ $i ] = trim($cc[$i]);
				}
			}
			else if( strpos($rawCC, ';') !== FALSE )	
			{
				$cc = explode( ';', $rawCC );
				$ccCnt = count($cc);
				for( $i=0; $i<$ccCnt; $i++ )
				{
					$cc[ $i ] = trim($cc[$i]);
				}
			}
			else
			{
				$cc = array(trim($rawCC));
			}
		}
		if( !empty($cc) )
		{
			if( is_array($cc) && count($cc)>0 )
			{
				foreach( $cc as $value )
				{
					$mailer->getMail()->addCc( $value );
				}
			}
			else
			{
				$mailer->getMail()->addCc( $cc );
			}
		}
		
		$rawBCC = $template->getBcc();
		$bcc = '';
		if( !empty($rawBCC) )
		{
			if( strpos($rawBCC, ',') !== FALSE )	
			{
				$bcc = explode( ',', $rawBCC );
				$bccCnt = count($bcc);
				for( $i=0; $i<$bccCnt; $i++ )
				{
					$bcc[ $i ] = trim($bcc[$i]);
				}
			}
			else if( strpos($rawBCC, ';') !== FALSE )	
			{
				$bcc = explode( ';', $rawBCC );
				$bccCnt = count($bcc);
				for( $i=0; $i<$bccCnt; $i++ )
				{
					$bcc[ $i ] = trim($bcc[$i]);
				}
			}
			else
			{
				$bcc = array(trim($rawBCC));
			}
		}
		if( !empty($bcc) )
		{
			$mailer->addBcc( $bcc );
		}
		
		$mailer->setTemplateStyles( $template->getEmailStyles() );
		
		$content = $template->getEmailContent();
		$subject = $template->getSubject();
		
		$vars = array(
			'ip' => '',
			'date' => '',
			'last_name' => '',
			'first_name' => '',
		);

		if ($this->isRegFieldsActive()) {
			$options = Mage::getModel('itoris_regfields/customer')->loadOptionsByCustomerId($customer->getId());
			$vars += $this->_prepareRegFieldsOptions($options);
		}
		
		$vars['ip'] = $customer->getIp();
		$vars['create_time'] 	= $customer->getDate();
		$vars['last_name'] = $user->getLastname();
		$vars['first_name'] = $user->getFirstname();
		$vars['email'] = $user->getEmail();

		foreach( $vars as $key => $value )
		{
			$key = '{{ipr_customer_'.$key.'}}';
			$content = str_replace( $key, $value, $content );
			$subject = str_replace( $key, $value, $subject );
		}
		
		$mailer->setTemplateSubject( $subject );
		$mailer->setTemplateText( $content );
		
		$mailer->setType(2);
		$emailVars = array(
			'customer' => $user,
		);
		if ($templateType != Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN) {
			$mailer->send($user->getEmail(), null, $emailVars);
		} else {
			$mailer->send($template->getAdminEmail(), null, $emailVars);
		}
	}

	protected function _prepareRegFieldsOptions($options) {
		$addressTypes = array('', 's_');
		foreach ($addressTypes as $prefix) {
			if (isset($options[$prefix . 'country_id'])) {
				/** @var Mage_Directory_Model_Country $country */
				$country = Mage::getModel('directory/country')->loadByCode($options[$prefix . 'country_id']);
				$options[$prefix . 'country_id'] = $country->getName();
				if (isset($options[$prefix . 'region_id'])) {
					$region = $country->getRegionCollection()->getItemById($options[$prefix . 'region_id']);
					if ($region) {
						$options[$prefix . 'region_id'] = $region->getName();
					}
				}
			}
		}

		if (isset($options['group_id'])) {
			$sourceModel = Mage::getModel('adminhtml/system_config_source_customer_group');
			$groups = $sourceModel->toOptionArray();
			foreach ($groups as $group) {
				if ($group['value'] == $options['group_id']) {
					$options['group_id'] = $group['label'];
				}
			}
		}

		return $options;
	}
	
	/* 
	 * 
	 * Check if all tables exists, and prefilled with neccessary records
	 * 
	 */
	public function checkConfiguration()
	{
		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton( 'core/resource' )->getConnection('core_write');
		
		$db->query( "
		CREATE TABLE IF NOT EXISTS `{$this->usersTableName}` (
		  `customer_id` int(11) NOT NULL,
		  `status` int(11) NOT NULL,
		  `ip` varchar(255) NOT NULL,
		  `date` datetime NOT NULL,
		  PRIMARY KEY (`customer_id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci
		" );
		
		$db->query( "
		CREATE TABLE IF NOT EXISTS `{$this->settingsTableName}` (
			`name` VARCHAR(255) NOT NULL,
			`value` VARCHAR(255) NOT NULL,
			PRIMARY KEY(`name`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci
		" );
		
		$db->query( "
		CREATE TABLE IF NOT EXISTS `{$this->templatesTableName}` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `from_name` varchar(255) NOT NULL,
		  `from_email` varchar(255) NOT NULL,
		  `subject` varchar(255) NOT NULL,
		  `cc` varchar(255) NOT NULL,
		  `bcc` varchar(255) NOT NULL,
		  `email_content` text NOT NULL,
		  `active` int(11) NOT NULL,
		  `email_styles` text NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci
		" );
		
		$value = $db->fetchOne("select `name` from `{$this->settingsTableName}` where name='active'");
		if ($value != 'active') {
			$db->query( "
				INSERT INTO {$this->settingsTableName}
				SET
					`name`='active',
					`value`=1
			" );
		}
		
		$value = intval($db->fetchOne("select `id` from `{$this->templatesTableName}` where id=1"));
		if ($value != 1) {
			// add template records
			$db->query( "
				INSERT INTO `{$this->templatesTableName}`
				SET
					`id` = 1,
					`subject` = 'New customer account created',
					`email_content` = '<p>
<p>Dear Admin!</p>
<p>A new customer account has been created. Currently it has a pending status.&nbsp;</p>
<p>Please login to admin area and activate the customer</p>
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>'
			");
			
			$db->query( "
				INSERT INTO `{$this->templatesTableName}`
				SET
					`id` = 2,
					`subject` = 'New customer account created',
					`email_content` = '<p>Dear {{ipr_customer_first_name}} {{ipr_customer_last_name}}!</p>
<p>your new account has been created. It need moderation and activation by administrators.&nbsp;</p>
<p>You will be informed additionally when your account is activated.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>'
			");
			$db->query( "
				INSERT INTO `{$this->templatesTableName}`
				SET
					`id` = 3,
					`subject` = 'Your account is approved',
					`email_content` = '<p>Dear {{ipr_customer_first_name}} {{ipr_customer_last_name}}!</p>
<p>Your account is approved. Please login using the credentials you have entered during the registration.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>'
			");
			$db->query( "
				INSERT INTO `{$this->templatesTableName}`
				SET
					`id` = 4,
					`subject` = 'Your account is declained',
					`email_content` = '<p>Dear {{ipr_customer_first_name}} {{ipr_customer_last_name}}!</p>
<p>Unfortunately your account registration &nbsp;has been declined. Please contact our administration for more information.</p>
<p>&nbsp;</p>'
			");
		}

		$columns = $db->fetchAll("show columns from `{$this->settingsTableName}`");
		if (strpos(print_r($columns, true), 'scope') === false) {
			$db->query("ALTER TABLE `{$this->settingsTableName}`
							ADD `scope` ENUM( 'default', 'website', 'store' ) NOT NULL DEFAULT 'default',
							ADD `scope_area` INT NOT NULL DEFAULT '0'");
		}
		$columns = $db->fetchAll("show columns from `{$this->templatesTableName}`");
		if (strpos(print_r($columns, true), 'scope') === false) {
			$db->query("ALTER TABLE `{$this->templatesTableName}` 
							ADD `scope` ENUM( 'default', 'website', 'store' ) NOT NULL DEFAULT 'default',
							ADD `scope_area` INT NOT NULL DEFAULT '0'");
		}
		if (strpos(print_r($columns, true), 'admin_email') === false) {
			// move admin_mail to the templates table from settings table
			$db->query("ALTER TABLE `{$this->templatesTableName}` ADD `admin_email` VARCHAR( 255 ) NOT NULL AFTER `from_email`");
			$value = $db->fetchOne("select `value` from `{$this->settingsTableName}` where name='admin_email'");

			$db->query("ALTER TABLE `{$this->templatesTableName}` ADD `type` INT NOT NULL AFTER `id`");
			$db->query("UPDATE `{$this->templatesTableName}` SET `type`=`id` ");
			$db->query("ALTER TABLE `{$this->settingsTableName}`
  							DROP PRIMARY KEY");
			$db->query("ALTER TABLE `{$this->settingsTableName}` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST ,
							ADD PRIMARY KEY ( `id` )");

			$db->query("UPDATE `{$this->templatesTableName}` SET `admin_email` = {$db->quote($value)} WHERE `type` = "
					   .Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN);

			$db->query("DELETE from `{$this->settingsTableName}` where `name`='admin_email'");
		}

	}
	
	public function isAdminRegistered(){
		try{
			return true;
		}catch(Exception $e){
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			return false;
		}
	}
	
	public function tryRegister()
	{
		if($this->request->isPost() && $this->request->getPost('registration', null) == 'true'){
			$sn = $this->request->getPost('sn', null);
			if($sn == null){
				Mage::getSingleton('adminhtml/session')->addError($this->__('Invalid serial number.'));
				return false;
			}
		
			$sn = trim($sn);
			try{
				$response = true;
				if($response == 0){
					Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The component has been registered!'));
					Mage::app()->cleanCache();
				}else{
					Mage::getSingleton('adminhtml/session')->addError($this->__('Invalid serial number!'));
				}
			}catch(Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			
		}
	}
	
	public function isRegisteredAutonomous(){
		return true;
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Scope
	 */
	public function getFrontendScope(){
		/** @var $scope Itoris_PendingRegistration_Model_Scope */
		$scope = Mage::getModel('itoris_pendingregistration/scope');
		$scope->setWebsiteId(Mage::app()->getWebsite()->getId());
		$scope->setStoreId(Mage::app()->getStore()->getId());
		return $scope;
	}

	/**
	 * @param Mage_Customer_Model_Customer $customer
	 * @return Itoris_PendingRegistration_Model_Scope
	 */
	public function getCustomerScope(Mage_Customer_Model_Customer $customer){
		/** @var $scope Itoris_PendingRegistration_Model_Scope */
		$scope = Mage::getModel('itoris_pendingregistration/scope');
		$scope->setStoreId($customer->getStoreId());
		$scope->setWebsiteId($customer->getWebsiteId());
		return $scope;
	}

	/**
	 * Check if Registration Fields Manager is installed and active
	 *
	 * @return bool
	 */
	public function isRegFieldsActive() {
		/** @var $config Mage_Core_Model_Config_Element */
		$config = Mage::getConfig()->getModuleConfig('Itoris_RegFields');
		return (bool)$config->active;
	}

	public function isStoreLoginControlRequireLogin() {
		/** @var $config Mage_Core_Model_Config_Element */
		$config = Mage::getConfig()->getModuleConfig('Itoris_StoreLoginControl');
		if ((bool)$config->active) {
			/** @var $helper Itoris_StoreLoginControl_Helper_Data */
			$helper = Mage::helper('itoris_stlc');
			return $helper->isMustLogin();
		}

		return false;
	}

	/**
	 * @static
	 * @return Itoris_PendingRegistration_Helper_Data
	 */
	public static function inst() {
		return Mage::helper('itoris_pendingregistration');
	}
}
?>