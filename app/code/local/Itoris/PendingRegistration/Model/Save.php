<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PendingRegistration_Model_Save {
	public function saveCustomer($params) {
		$customer = $params->getCustomer();
		if ($customer->getCanSendItorisEmail()) {
			/** @var $helper Itoris_PendingRegistration_Helper_Data */
			$helper = Mage::helper('itoris_pendingregistration/data');
			$scope = $helper->getCustomerScope($customer);

			if (!$helper->isAdminRegistered()) {
				return;
			}

			if (!Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($scope)){
				return;
			}

			$data = Mage::app()->getRequest()->getParam('account_admin');
			/** @var $db Varien_Db_Adapter_Pdo_Mysql */
			$db = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );
			$usersTableName = Mage::getSingleton('core/resource')->getTableName('itoris_pendingregistration_users');
			$entity_id = $customer->getEntityId();

			$result = $db->query( 'SELECT status FROM '.$usersTableName.' WHERE customer_id='.$entity_id );
			$current_status = intval($result->fetchColumn(0));

			$newStatus = $customer->getConfirmation() && $customer->isConfirmationRequired() ? Itoris_PendingRegistration_Model_Users::STATUS_NOT_CONFIRMED_BY_EMAIL : intval($data['status']);

			$db->query('UPDATE '.$usersTableName.' SET status='.$newStatus.' WHERE customer_id='.$entity_id);

			if($current_status!=$data[ 'status' ] && $data[ 'status' ] != 0) {
				if ($data[ 'status' ] == 1) {
					$templateId = Itoris_PendingRegistration_Model_Template::$EMAIL_APPROVED;
				} else if($data[ 'status' ] == 2) {
					$templateId = Itoris_PendingRegistration_Model_Template::$EMAIL_DECLAINED;
				}
				$helper->sendEmail($templateId, $customer, $scope);
			}
			if (!$customer->getConfirmation()) {
				//it's won't saved, just disallow welcome email
				$customer->setConfirmation(true);
			}
		}
	}

	public function setItorisFlag($observer) {
		$customer = $observer->getCustomer();
		$customer->setCanSendItorisEmail(true);
		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper('itoris_pendingregistration/data');
		$helper->init(null);
	}
}
?>