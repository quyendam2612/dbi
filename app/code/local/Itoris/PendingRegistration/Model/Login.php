<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */
class Itoris_PendingRegistration_Model_Login {

	public function checkLoginAbility($params) {
		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper( 'itoris_pendingregistration' );
		if (!$helper->isRegisteredAutonomous()) {
			return true;
		}

		$scope = $helper->getFrontendScope();
		if (!Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($scope)) {
			return true;
		}
			
		$db = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );
		$cid = Mage::getSingleton('customer/session')->getCustomer()->getId();
			
		$usersTableName = Mage::getSingleton('core/resource')->getTableName('itoris_pendingregistration_users');
		$status = $db->query('SELECT status FROM '.$usersTableName.' WHERE customer_id='.$cid);
		$status = $status->fetchColumn(0);
		$error = false;
		if (!$status) {
			$error = $helper->__('Your account requires moderation');
		} elseif ($status == 2) {
			$error = $helper->__('Your registration has been declined. Please contact the site administrator for more details');
		} elseif ($status == Itoris_PendingRegistration_Model_Users::STATUS_NOT_CONFIRMED_BY_EMAIL) {
			/** @var $customer Mage_Customer_Model_Customer */
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			if ($customer->getConfirmation() && $customer->isConfirmationRequired()) {
				$error = Mage::getSingleton('customer/session')->addSuccess(Mage::helper('customer')->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
			} else {
				$db->query('UPDATE '.$usersTableName.' SET status=0 WHERE customer_id='.$cid);
				$helper->sendEmail(Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN, $customer, $scope);
				$helper->sendEmail(Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_USER, $customer, $scope);
				$error = $helper->__('Your account requires moderation');
			}
		}
		if ($error) {
			/** @var $session Mage_Customer_Model_Session */
			$session = Mage::getSingleton('customer/session');
			$session->addError($error);
			$session->setCustomer(Mage::getModel('customer/customer'));
			if ($this->getDataHelper()->isStoreLoginControlRequireLogin() || $session->getIsLoginViaSmartLogin()) {
				return;
			}
			Mage::app()->getResponse()->setRedirect(Mage::getUrl('*/*/login'));
			Mage::app()->getResponse()->sendHeaders();exit;
		}

		Mage::getSingleton('customer/session')->getCustomer()->setIsJustConfirmed(false);
		return true;
	}

	/**
	 * @return Itoris_PendingRegistration_Helper_Data
	 */
	public function getDataHelper() {
		return Mage::helper('itoris_pendingregistration/data');
	}
}
?>