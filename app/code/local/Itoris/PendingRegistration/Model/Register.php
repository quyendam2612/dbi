<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Model_Register extends Mage_Core_Model_Abstract
{

	protected $showSuccessPendingMessage = false;

	protected function _filterDates($array, $dateFields)
	{
		if (empty($dateFields)) {
		    return $array;
		}
		$filterInput = new Zend_Filter_LocalizedToNormalized(array(
		    'date_format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
		));
		$filterInternal = new Zend_Filter_NormalizedToLocalized(array(
		    'date_format' => Varien_Date::DATE_INTERNAL_FORMAT
		));

		foreach ($dateFields as $dateField) {
		    if (array_key_exists($dateField, $array) && !empty($dateField)) {
			$array[$dateField] = $filterInput->filter($array[$dateField]);
			$array[$dateField] = $filterInternal->filter($array[$dateField]);
		    }
		}
		return $array;
	}

	public function createCustomer($controller = null) {
		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper('itoris_pendingregistration');
		/** @var $session Mage_Customer_Model_Session */
		$session = Mage::getSingleton('customer/session');
		if ($session->isLoggedIn()) {
		    return;
		}
		$session->setEscapeMessages(true); // prevent XSS injection in user input
		if (Mage::app()->getRequest()->isPost()) {
			$errors = array();

			if (!$customer = Mage::registry('current_customer')) {
				$customer = Mage::getModel('customer/customer')->setId(null);
			}

			$data = $this->_filterDates(Mage::app()->getRequest()->getPost(), array('dob'));

			foreach (Mage::getConfig()->getFieldset('customer_account') as $code=>$node) {
				if ($node->is('create') && isset($data[$code])) {
				    if ($code == 'email') {
					$data[$code] = trim($data[$code]);
				    }
				    $customer->setData($code, $data[$code]);
				}
			}

			if (Mage::app()->getRequest()->getParam('is_subscribed', false)) {
				$customer->setIsSubscribed(1);
			}

			/**
			* Initialize customer group id
			*/
			$customer->getGroupId();

			if (Mage::app()->getRequest()->getPost('create_address')) {
				$address = Mage::getModel('customer/address')
				    ->setData(Mage::app()->getRequest()->getPost())
				    ->setIsDefaultBilling(Mage::app()->getRequest()->getParam('default_billing', false))
				    ->setIsDefaultShipping(Mage::app()->getRequest()->getParam('default_shipping', false))
				    ->setId(null);
				$customer->addAddress($address);

				$errors = $address->validate();
				if (!is_array($errors)) {
				    $errors = array();
				}
			}

			try {
				$validationCustomer = $customer->validate();
				if (is_array($validationCustomer)) {
				    $errors = array_merge($validationCustomer, $errors);
				}
				$validationResult = count($errors) == 0;

				if (true === $validationResult) {
					$customer->save();
					Mage::dispatchEvent('customer_register_success',
						array('account_controller' => $controller, 'customer' => $customer)
					);
					if ($customer->getConfirmation() && $customer->isConfirmationRequired()) {
						$customer->sendNewAccountEmail(
							'confirmation',
							$session->getBeforeAuthUrl(),
							Mage::app()->getStore()->getId()
						);
						$session->addSuccess(Mage::helper('customer')->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
					} else {
						$this->showSuccessPendingMessage = true;
					}
				} else {
					$session->setCustomerFormData(Mage::app()->getRequest()->getPost());
					if (is_array($errors)) {
						foreach ($errors as $errorMessage)  {
							$session->addError($errorMessage);
						}
					} else {
						$session->addError($helper->__('Invalid customer data'));
					}
				}
			} catch (Mage_Core_Exception $e) {
				$session->setCustomerFormData(Mage::app()->getRequest()->getPost());
				if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
				    $url = Mage::getUrl('customer/account/forgotpassword');
				    $message = $helper->__('There is already an account with this emails address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
				    $session->setEscapeMessages(false);
				} else {
				    $message = $e->getMessage();
				}
				$session->addError($message);
			} catch (Exception $e) {
				$session->setCustomerFormData(Mage::app()->getRequest()->getPost())
				    ->addException($e, $helper->__('Can\'t save customer'));
			}
		}
	}

	public function createCustomer13($controller = null) {
		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper('itoris_pendingregistration');
		
		$session = Mage::getSingleton('customer/session');
			if ($session->isLoggedIn()) {
		    return;
		}
		if (Mage::app()->getRequest()->isPost()) {
		    $errors = array();

		    if (!$customer = Mage::registry('current_customer')) {
				$customer = Mage::getModel('customer/customer')->setId(null);
		    }

		    foreach (Mage::getConfig()->getFieldset('customer_account') as $code=>$node) {
			if ($node->is('create') && ($value = Mage::app()->getRequest()->getParam($code)) !== null) {
			    $customer->setData($code, $value);
			}
		    }

		    if (Mage::app()->getRequest()->getParam('is_subscribed', false)) {
				$customer->setIsSubscribed(1);
		    }

		    /**
		     * Initialize customer group id
		     */
		    $customer->getGroupId();

		    if (Mage::app()->getRequest()->getPost('create_address')) {
			$address = Mage::getModel('customer/address')
			    ->setData(Mage::app()->getRequest()->getPost())
			    ->setIsDefaultBilling(Mage::app()->getRequest()->getParam('default_billing', false))
			    ->setIsDefaultShipping(Mage::app()->getRequest()->getParam('default_shipping', false))
			    ->setId(null);
			$customer->addAddress($address);

			$errors = $address->validate();
			if (!is_array($errors)) {
			    $errors = array();
			}
		    }

		    try {
				$validationCustomer = $customer->validate();
			if (is_array($validationCustomer)) {
			    $errors = array_merge($validationCustomer, $errors);
			}
			$validationResult = count($errors) == 0;

			if (true === $validationResult) {
			    $customer->save();
				Mage::dispatchEvent('customer_register_success',
					array('account_controller' => $controller, 'customer' => $customer)
				);
				if ($customer->getConfirmation() && $customer->isConfirmationRequired()) {
					$customer->sendNewAccountEmail('confirmation', Mage::getSingleton('customer/session')->getBeforeAuthUrl());
					Mage::getSingleton('customer/session')->addSuccess(Mage::helper('customer')->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
				} else {
					$this->showSuccessPendingMessage = true;
				}
			    return;
			} else {
			    $session->setCustomerFormData(Mage::app()->getRequest()->getPost());
			    if (is_array($errors)) {
				foreach ($errors as $errorMessage) {
				    $session->addError($errorMessage);
				}
			    }
			    else {
				$session->addError($helper->__('Invalid customer data'));
			    }
			}
		    }
		    catch (Mage_Core_Exception $e) {
			$session->addError($e->getMessage())
			    ->setCustomerFormData(Mage::app()->getRequest()->getPost());
		    }
		    catch (Exception $e) {
			$session->setCustomerFormData(Mage::app()->getRequest()->getPost())
			    ->addException($e, $helper->__('Can\'t save customer'));
		    }
		}
		
		$session->setEscapeMessages(true);
	}

	public function sendEmails($params) {
		if (empty($_POST[ 'email' ])) {
			return;
		}

		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper( 'itoris_pendingregistration/data' );
		$helper->init( null );
		$scope = $helper->getFrontendScope();
		
		if (!Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($scope)) {
			return;
		}
	
		if (!$helper->isRegisteredAutonomous()) {
			return;
		}
		$controller = null;
		if ($params instanceof Varien_Event_Observer) {
			$controller = $params->getData('controller_action');
		}
		$this->showSuccessPendingMessage = false;
		if(method_exists('Mage','getVersionInfo')) {
			if ($helper->isRegFieldsActive()) {
				return;
			}
			$this->createCustomer($controller);
		} else {
			$this->createCustomer13($controller);
		}

		$status = $this->writePendingStatusInDb($_POST[ 'email' ], $scope, $helper);
		if ($status != 1 && $this->showSuccessPendingMessage) {
			Mage::getSingleton('customer/session')->addSuccess($helper->__('Thank you for registration. Your account requires moderation before you can login.'));
		}
		$helper->customerLogout();
	}

	private function writePendingStatusInDb($email, $scope, Itoris_PendingRegistration_Helper_Data $helper) {
		/** @var $db Varien_Db_Adapter_Pdo_Mysql */
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');

		$usersTableName = Mage::getModel('core/resource')->getTableName('itoris_pendingregistration_users');
		$customersTableName = Mage::getModel('core/resource')->getTableName('customer_entity');
		$websiteId = (int)Mage::app()->getWebsite()->getId();
		$data = $db->fetchRow('SELECT * FROM '.$customersTableName.' WHERE email=? and website_id='.$websiteId, $email);

		if (isset($data['email'])) {
			$id = intval($data[ 'entity_id' ]);

			$isExists = (boolean) $db->fetchOne("SELECT COUNT(*) FROM ".$usersTableName." WHERE customer_id=?", $id);
			/** @var $user Mage_Customer_Model_Customer */
			$user = Mage::getModel('customer/customer')->load($id);
			$status = $user->getConfirmation() && $user->isConfirmationRequired()
				? Itoris_PendingRegistration_Model_Users::STATUS_NOT_CONFIRMED_BY_EMAIL
				: Itoris_PendingRegistration_Model_Users::STATUS_PENDING;
			$groups = Mage::getModel('itoris_pendingregistration/customerGroup')->getGroups($websiteId, Mage::app()->getStore()->getId());
			if (!empty($groups) && !in_array($user->getGroupId(), $groups)) {
				$status = Itoris_PendingRegistration_Model_Users::STATUS_APPROVED;
			}

			if (!$isExists) {
				$db->query("INSERT INTO $usersTableName SET customer_id=?, status={$status}, ip=?, date=CURRENT_TIMESTAMP()",
					array($id, $_SERVER['REMOTE_ADDR']));

				if (!$status) {
					$helper->sendEmail(Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_ADMIN, $user, $scope);
					$helper->sendEmail(Itoris_PendingRegistration_Model_Template::$EMAIL_REG_TO_USER, $user, $scope);
				}
			}
			return $status;
		}
		return null;
	}

	/**
	 * Observer for customer_register_success action
	 *
	 * @param $params
	 * @return mixed
	 */
	public function registerSuccess($params) {
		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper( 'itoris_pendingregistration/data' );

		if (!($helper->isRegFieldsActive() || $helper->isStoreLoginControlRequireLogin())) {
			return;
		}
		/** @var $controller Itoris_RegFields_AccountController */
		$controller = $params->getAccountController();
		/** @var $customer Mage_Customer_Model_Customer */
		$customer = $params->getCustomer();

		if (!$controller->getRequest()->getParam('email')) {
			return;
		}

		$helper->init(null);
		$scope = $helper->getFrontendScope();

		if (!Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($scope)) {
			return;
		}

		if (!$helper->isRegisteredAutonomous()) {
			return;
		}
		$session = Mage::getSingleton('customer/session');
		$message = null;
		if ($customer->getConfirmation() && $customer->isConfirmationRequired()) {
			$customer->sendNewAccountEmail(
				'confirmation',
				$session->getBeforeAuthUrl(),
				Mage::app()->getStore()->getId()
			);
			$message = Mage::helper('customer')->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail()));
			$session->addSuccess($message);
			$message = null;
		} else {
			$message = $helper->__('Thank you for registration. Your account requires moderation before you can login.');
		}

		$status = $this->writePendingStatusInDb($controller->getRequest()->getParam('email'), $scope, $helper);
		if ($status != Itoris_PendingRegistration_Model_Users::STATUS_APPROVED && $message) {
			$session->addSuccess($message);
		}
		if ($status == Itoris_PendingRegistration_Model_Users::STATUS_APPROVED && $controller instanceof Itoris_RegFields_AccountController) {
			return;
		}
		if ($helper->isStoreLoginControlRequireLogin() || $this->getCustomerSession()->getSmartLoginRegistration()) {
			if ($status != Itoris_PendingRegistration_Model_Users::STATUS_APPROVED) {
				$customer->setPendingMessage($message);
			}
		} else {
			$helper->customerLogout();
		}
	}

	public function addPendingStatusToCustomer($customer) {
		/** @var $helper Itoris_PendingRegistration_Helper_Data */
		$helper = Mage::helper('itoris_pendingregistration/data');
		$helper->init(null);
		$scope = $helper->getFrontendScope();
		if (!Itoris_PendingRegistration_Model_Settings::inst()->isEngineActive($scope)) {
			return null;
		}
		if (!$helper->isRegisteredAutonomous()) {
			return null;
		}

		$status = $this->writePendingStatusInDb($customer->getEmail(), $scope, $helper);

		if ($status != 1) {
			$customer->setPendingMessage($helper->__('Thank you for registration. Your account requires moderation before you can login.'));
		}

		return $customer;
	}

	/**
	 * Retrieve customer session model object
	 *
	 * @return Mage_Customer_Model_Session
	 */
	protected function getCustomerSession() {
		return Mage::getSingleton('customer/session');
	}

}?>