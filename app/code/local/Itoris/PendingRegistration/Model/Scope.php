<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Model_Scope extends Mage_Core_Model_Abstract{

	public function __construct(){
		parent::__construct();
		$this->scope = array(
			self::$CONFIGURATION_SCOPE_STORE => null,
			self::$CONFIGURATION_SCOPE_WEBSITE => null,
			self::$CONFIGURATION_SCOPE_DEFAULT => 0
		);
	}

	public function setStoreCode($code){
		if($code !== null && !is_numeric($code)){
			$store = Mage::getModel('core/store')->load($code);
			$this->setStoreId((int) $store->getId());
		}else{
			$this->setStoreId($code);
		}
	}

	public function setStoreId($id){
		$this->scope[self::$CONFIGURATION_SCOPE_STORE] = $id;
	}

	public function setWebsiteCode($code){
		if($code !== null && !is_numeric($code)){
			$website = Mage::getModel('core/website')->load($code);
			$this->setWebsiteId((int) $website->getId());
		}else{
			$this->setWebsiteId($code);
		}
	}

	public function setWebsiteId($id){
		$this->scope[self::$CONFIGURATION_SCOPE_WEBSITE] = $id;
	}

	public function getStoreId(){
		return $this->scope[self::$CONFIGURATION_SCOPE_STORE];
	}

	public function getWebsiteId(){
		return $this->scope[self::$CONFIGURATION_SCOPE_WEBSITE];
	}

	public function getDefault(){
		return $this->scope[self::$CONFIGURATION_SCOPE_DEFAULT];
	}

	public function setDefault($id){
		$this->scope[self::$CONFIGURATION_SCOPE_DEFAULT] = $id;
	}

	protected function getScope(){
		return $this->scope;
	}

	public function getSummary(){
		return $this->scope;
	}

	public function getSummaryForUrl(){
		$scope = $this->scope;
		unset($scope[self::$CONFIGURATION_SCOPE_DEFAULT]);

		if($scope[self::$CONFIGURATION_SCOPE_STORE] !== null){
			/** @var $store Mage_Core_Model_Store */
			$store = Mage::getModel('core/store')->load($scope[self::$CONFIGURATION_SCOPE_STORE]);
			$scope[self::$CONFIGURATION_SCOPE_STORE] = $store->getCode();
		}

		if($scope[self::$CONFIGURATION_SCOPE_WEBSITE] !== null){
			/** @var $website Mage_Core_Model_Website */
			$website = Mage::getModel('core/website')->load($scope[self::$CONFIGURATION_SCOPE_WEBSITE]);
			$scope[self::$CONFIGURATION_SCOPE_WEBSITE] = $website->getCode();
		}

		return $scope;
	}

	public function getTightScope(){
		/** @var $tight Itoris_PendingRegistration_Model_Scope */
		$tight = Mage::getModel('itoris_pendingregistration/scope');
		$tight->setDefault(null);

		if($this->getStoreId() !== null){
			$tight->setStoreId($this->getStoreId());
			return $tight;
		}

		if($this->getWebsiteId() !== null){
			$tight->setWebsiteId($this->getWebsiteId());
			return $tight;
		}

		$tight->setDefault(0);
		return $tight;
	}

	public function getTightType(){
		$pair = $this->getTightPair();
		$pair = array_keys($pair);
		return $pair[0];
	}

	public function getTightArea(){
		$pair = $this->getTightPair();
		$pair = array_values($pair);
		return $pair[0];
	}

	protected function getTightPair(){
		foreach($this->scope as $type => $area){
			if($area !== null){
				return array($type => $area);
			}
		}
		throw new Exception("Invalid scope object");
	}

	public static function getWhereSql(Itoris_PendingRegistration_Model_Scope $scope){
		$scopeData = $scope->getScope();

		/** @var $resource Mage_Core_Model_Resource */
		$resource = Mage::getSingleton('core/resource');
		/** @var $read Varien_Db_Adapter_Pdo_Mysql */
		$read = $resource->getConnection('core_read');

		$scopeWhere = array();
		foreach($scopeData as $scopeType => $scopeArea){
			if($scopeArea !== null){
				$scopeWhere[] = "(scope={$read->quote($scopeType)} and scope_area={$scopeArea})";
			}
		}
		$scopeWhere = implode($scopeWhere, ' or ');
		return $scopeWhere;
	}

	private $scope;

	public static $CONFIGURATION_SCOPE_DEFAULT = 'default';
	public static $CONFIGURATION_SCOPE_WEBSITE = 'website';
	public static $CONFIGURATION_SCOPE_STORE = 'store';
}
?>