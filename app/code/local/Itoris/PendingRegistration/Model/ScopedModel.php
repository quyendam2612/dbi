<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PENDINGREGISTRATION
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PendingRegistration_Model_ScopedModel extends Mage_Core_Model_Abstract{

	public function load($id, $field = null, Itoris_PendingRegistration_Model_Scope $scope = null){
		$this->_getResource()->load($this, $id, $field, $scope);
		return $this;
	}

		/**
	 *
	 * @param Itoris_PendingRegistration_Model_Scope $scope
	 * @return void
	 */
	public function setScope(Itoris_PendingRegistration_Model_Scope $scope){
		$this->setData('scope', $scope->getTightType());
		$this->setData('scope_area', $scope->getTightArea());
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Scope
	 */
	public function getScope(){
		/** @var $scope Itoris_PendingRegistration_Model_Scope */
		$scope = Mage::getModel('itoris_pendingregistration/scope');
		$scope->setDefault(null);

		$type = $this->getData('scope');
		$typeArea = $this->getData('scope_area');
		if($type == Itoris_PendingRegistration_Model_Scope::$CONFIGURATION_SCOPE_STORE){
			$scope->setStoreId($typeArea);
		}
		if($type == Itoris_PendingRegistration_Model_Scope::$CONFIGURATION_SCOPE_WEBSITE){
			$scope->setWebsiteId($typeArea);
		}
		if($type == Itoris_PendingRegistration_Model_Scope::$CONFIGURATION_SCOPE_DEFAULT){
			$scope->setDefault($typeArea);
		}
		return $scope;
	}

	/**
	 * @return Itoris_PendingRegistration_Model_Mysql4_ScopedModel
	 */
	protected function _getResource(){
		return parent::_getResource();
	}

	public function recordExists($id, $field = null, Itoris_PendingRegistration_Model_Scope $scope = null){
		return $this->_getResource()->recordExists($id, $field, $scope);
	}

}
?>